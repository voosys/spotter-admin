<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class AppController extends Controller {

    private $sitePath;

    private function recordAnalytics($flipBookId, $analyticsType, $sourceUrl, $ipAddress) {

        $id = DB::table('analytics')->insertGetId(['flipbook_id' => $flipBookId, 'analytics_type' => $analyticsType, 'source_url' => $sourceUrl, 'ip_address' => $ipAddress, 'created_at' => time()]);

        if ($id) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public function __construct() {

        $this->sitePath = config('app.site_path');
        $this->userDetails = session()->get('userDetails');
    }

    public function companies(Request $request) {

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        if (in_array($this->userDetails[0]->user_role, array("Admin"))) {

            return view('company/index', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        } else {

            return view('errors/unauthorized', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        }
    }

    public function points(Request $request) {

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        if (in_array($this->userDetails[0]->user_role, array("Admin", "Manager", "Editor"))) {

            $points = DB::table('points')
                    ->selectRaw('points.id as id, points.point_name as point_name, points.point_url as point_url, points.point_latitude as point_latitude, points.point_longitude as point_longitude, points.point_active as point_active, points.created_at as point_created_at, points.updated_at as point_updated_at')
                    ->orderBy('point_updated_at', 'desc')
                    ->get();

            return view('point/index', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'points' => $points]);
        } else {

            return view('errors/unauthorized', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        }
    }

    public function pointTypes(Request $request) {

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        if (in_array($this->userDetails[0]->user_role, array("Admin", "Manager", "Editor"))) {

            $pointTypes = DB::table('point_types')
                    ->selectRaw('point_types.id as id, point_types.point_type_name as point_type_name, point_types.point_type_active as point_type_active, point_types.created_at as point_type_created_at, point_types.updated_at as point_type_updated_at')
                    ->orderBy('point_type_updated_at', 'desc')
                    ->get();

            return view('point-type/index', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'pointTypes' => $pointTypes]);
        } else {

            return view('errors/unauthorized', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        }
    }

    public function notifications(Request $request) {

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        if (in_array($this->userDetails[0]->user_role, array("Admin", "Manager", "Editor"))) {

            return view('notification/index', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        } else {

            return view('errors/unauthorized', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        }
    }

    public function permissions(Request $request) {

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        if (in_array($this->userDetails[0]->user_role, array("Admin"))) {

            return view('permission/index', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        } else {

            return view('errors/unauthorized', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        }
    }

    public function users(Request $request) {

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        if (in_array($this->userDetails[0]->user_role, array("Admin"))) {

            return view('user/index', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        } else {

            return view('errors/unauthorized', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
        }
    }

    public function track(Request $request) {

        $flipBookId = $request->input('id');
        $analyticsType = $request->input('type');
        $sourceUrl = $request->input('url');
        $ipAddress = $request->ip();

        $this->recordAnalytics($flipBookId, $analyticsType, $sourceUrl, $ipAddress);
    }

}
