<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CompanyController extends Controller {

    private $sitePath;

    public function __construct() {

        $this->sitePath = config('app.site_path');
    }

    public function add(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        return view('company/company-add', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
    }

    public function delete(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companyIndex = $request->input('companyIndex');

        if ($this->userDetails[0]->company_id == $companyIndex) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => ['Unable to delete the company as the company is currently in use.']]);
        } else {

            try {

                $users = DB::table('users')
                        ->selectRaw('users.id as id')
                        ->where('users.company_id', $companyIndex)
                        ->get();

                if (count($users) > 0) {

                    return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => ['Unable to delete the company as there are users in the company. Please remove all the users and try again.']]);
                } else {

                    $files = glob('./uploads/companies/' . $companyIndex . '/*');

                    foreach ($files as $file) {

                        if (is_file($file)) {

                            unlink($file);
                        }
                    }

                    rmdir('./uploads/companies/' . $companyIndex);

                    $rows = DB::table('companies')
                            ->where(['id' => $companyIndex])
                            ->delete();

                    if ($rows) {

                        return response()->json(['status' => 'SUCCESS', 'message' => 'Company deleted successfully.']);
                    } else {

                        return response()->json(['status' => 'FAILED', 'message' => 'Company already deleted.']);
                    }
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function save(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $validation = \Validator::make(
                        array(
                    'company_name' => Input::get('companyName'),
                    'company_website_url' => Input::get('companyWebsiteURL'),
                    'company_email_address' => Input::get('companyEmailAddress'),
                    'company_phone_number' => Input::get('companyPhoneNumber'),
                    'company_logo' => $request->file('companyLogo'),
                        ), array(
                    'company_name' => 'required|string',
                    'company_website_url' => 'required|url',
                    'company_email_address' => 'required|email',
                    'company_phone_number' => 'required|string',
                    'company_logo' => 'required|image',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            $companyName = $request->input('companyName');
            $companyWebsiteURL = $request->input('companyWebsiteURL');
            $companyEmailAddress = $request->input('companyEmailAddress');
            $companyPhoneNumber = $request->input('companyPhoneNumber');
            $companyActive = $request->input('companyActive');
            $companyLogo = NULL;

            if ($request->hasFile('companyLogo') && $request->file('companyLogo')->isValid()) {

                $extension = $request->file('companyLogo')->getClientOriginalExtension();

                $fileName = md5(time()) . '.' . $extension;

                try {

                    $id = DB::table('companies')->insertGetId(['company_name' => $companyName, 'company_website_url' => $companyWebsiteURL, 'company_email_address' => $companyEmailAddress, 'company_phone_number' => $companyPhoneNumber, 'company_active' => $companyActive, 'created_at' => time()]);

                    if ($id) {

                        $request->file('companyLogo')->move('./uploads/companies/' . $id, $fileName);

                        $status = DB::table('companies')
                                ->where('id', $id)
                                ->update(['company_logo' => $fileName]);

                        if ($status == 1) {

                            return response()->json(['status' => 'SUCCESS', 'message' => 'Company created successfully.']);
                        } else {

                            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                        }
                    } else {

                        return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                    }
                } catch (\Exception $e) {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            }
        }
    }

    public function view(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $company = DB::table('companies')
                ->selectRaw('companies.id as id, companies.company_name as company_name, companies.company_website_url as company_website_url, companies.company_email_address as company_email_address, companies.company_phone_number as company_phone_number, companies.company_active as company_active, companies.created_at as company_created_at, companies.updated_at as company_updated_at')
                ->where('companies.id', $request->id)
                ->get();

        $company[0]->company_created_at = date('d-m-Y', $company[0]->company_created_at);
        $company[0]->company_updated_at = date('d-m-Y', $company[0]->company_updated_at);

        return view('company/company-details', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'company' => $company]);
    }

    public function update(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companyIndex = $request->input('companyIndex');
        $companyName = $request->input('companyName');
        $companyWebsiteURL = $request->input('companyWebsiteURL');
        $companyEmailAddress = $request->input('companyEmailAddress');
        $companyPhoneNumber = $request->input('companyPhoneNumber');
        $companyActive = $request->input('companyActive');
        $companyLogo = NULL;

        try {

            $status = DB::table('companies')
                    ->where('id', $companyIndex)
                    ->update(['company_name' => $companyName, 'company_website_url' => $companyWebsiteURL, 'company_email_address' => $companyEmailAddress, 'company_phone_number' => $companyPhoneNumber, 'company_active' => $companyActive, 'updated_at' => time()]);

            if ($status == 1) {

                if ($request->hasFile('companyLogo') && $request->file('companyLogo')->isValid()) {

                    $files = glob('./uploads/companies/' . $companyIndex . '/*');

                    foreach ($files as $file) {

                        if (is_file($file)) {

                            unlink($file);
                        }
                    }

                    $extension = $request->file('companyLogo')->getClientOriginalExtension();

                    $fileName = md5(time()) . '.' . $extension;

                    $request->file('companyLogo')->move('./uploads/companies/' . $companyIndex, $fileName);

                    $status = DB::table('companies')
                            ->where('id', $companyIndex)
                            ->update(['company_logo' => $fileName, 'updated_at' => time()]);
                }

                return response()->json(['status' => 'SUCCESS', 'message' => 'Company updated successfully.']);
            } else {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        } catch (\Exception $e) {

            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
        }
    }

    public function viewAll(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companies = DB::table('companies')
                ->selectRaw('companies.id as id, companies.company_name as company_name, companies.company_active as company_active, companies.created_at as company_created_at, companies.updated_at as company_updated_at')
                ->get();

        foreach ($companies as &$company) {

            $company->company_created_at = date('d-m-Y', $company->company_created_at);
            $company->company_updated_at = date('d-m-Y', $company->company_updated_at);
        }

        if (count($companies) > 0) {

            return response()->json(['data' => $companies]);
        } else {

            return response()->json(['status' => 'FAILED', 'message' => 'Invalid request.']);
        }
    }

}
