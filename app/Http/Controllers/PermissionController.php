<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class PermissionController extends Controller {

    private $sitePath;

    public function __construct() {

        $this->sitePath = config('app.site_path');
    }

    public function add(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companies = DB::table('companies')
                ->selectRaw('companies.id as id, companies.company_name as company_name, companies.company_active as company_active')
                ->where('companies.company_active', 'Yes')
                ->orderBy('company_name', 'ASC')
                ->get();

        $users = DB::table('users')
                ->selectRaw('users.id as id, users.user_username as user_username, users.user_first_name as user_first_name, users.user_last_name as user_last_name')
                ->where('users.user_active', 'Yes')
                ->orderBy('user_username', 'ASC')
                ->get();

        return view('permission/permission-add', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'companies' => $companies, 'users' => $users]);
    }
    
    public function delete(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $permissionIndex = $request->input('permissionIndex');
        
        try {

            $rows = DB::table('permissions')
                    ->where(['id' => $permissionIndex])
                    ->delete();

            if ($rows) {

                return response()->json(['status' => 'SUCCESS', 'message' => 'Permission deleted successfully.']);
            } else {

                return response()->json(['status' => 'FAILED', 'message' => 'Permission already deleted.']);
            }
        } catch (\Exception $e) {

            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
        }
    }

    public function save(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companyIndex = $request->input('companyIndex');
        $userIndex = $request->input('userIndex');
        $permissionRead = $request->input('permissionRead');
        $permissionWrite = $request->input('permissionWrite');
        $permissionDelete = $request->input('permissionDelete');

        isset($permissionRead) ? $permissionRead = "Yes" : $permissionRead = "No";
        isset($permissionWrite) ? $permissionWrite = "Yes" : $permissionWrite = "No";
        isset($permissionDelete) ? $permissionDelete = "Yes" : $permissionDelete = "No";

        $permission = DB::table('permissions')
                ->selectRaw('permissions.id as id')
                ->where('permissions.company_id', $companyIndex)
                ->where('permissions.user_id', $userIndex)
                ->get();

        if (count($permission) > 0) {

            return response()->json(['status' => 'FAILED', 'message' => 'The specified permission already exists.']);
        } else {

            try {

                $id = DB::table('permissions')->insertGetId(['company_id' => $companyIndex, 'user_id' => $userIndex, 'permission_read' => $permissionRead, 'permission_write' => $permissionWrite, 'permission_delete' => $permissionDelete, 'created_at' => time()]);

                if ($id) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'permission created successfully.']);
                } else {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function update(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $permissionIndex = $request->input('permissionIndex');
        $permissionRead = $request->input('permissionRead');
        $permissionWrite = $request->input('permissionWrite');
        $permissionDelete = $request->input('permissionDelete');
        
        isset($permissionRead) ? $permissionRead = "Yes" : $permissionRead = "No";
        isset($permissionWrite) ? $permissionWrite = "Yes" : $permissionWrite = "No";
        isset($permissionDelete) ? $permissionDelete = "Yes" : $permissionDelete = "No";

        try {

            $status = DB::table('permissions')
                    ->where('id', $permissionIndex)
                    ->update(['permission_read' => $permissionRead, 'permission_write' => $permissionWrite, 'permission_delete' => $permissionDelete, 'updated_at' => time()]);

            if ($status == 1) {

                return response()->json(['status' => 'SUCCESS', 'message' => 'Permission updated successfully.']);
            } else {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        } catch (\Exception $e) {

            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
        }
    }

    public function view(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $permission = DB::table('permissions')
                ->join('companies', 'permissions.company_id', '=', 'companies.id')
                ->join('users', 'permissions.user_id', '=', 'users.id')
                ->selectRaw('permissions.id as id, companies.company_name as company_name, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, permissions.permission_read as permission_read, permissions.permission_write as permission_write, permissions.permission_delete as permission_delete, permissions.created_at as permission_created_at, permissions.updated_at as permission_updated_at')
                ->where('permissions.id', $request->input('id'))
                ->get();

        $permission[0]->user_fullname = $permission[0]->user_first_name . ' ' . $permission[0]->user_last_name;
        $permission[0]->permission_created_at = date('d-m-Y', $permission[0]->permission_created_at);
        $permission[0]->permission_updated_at = date('d-m-Y', $permission[0]->permission_updated_at);

        return view('permission/permission-details', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'permission' => $permission]);
    }

    public function viewAll(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $permissions = DB::table('permissions')
                ->join('companies', 'permissions.company_id', '=', 'companies.id')
                ->join('users', 'permissions.user_id', '=', 'users.id')
                ->selectRaw('permissions.id as id, companies.company_name as company_name, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, permissions.permission_read as permission_read, permissions.permission_write as permission_write, permissions.permission_delete as permission_delete, permissions.created_at as permission_created_at, permissions.updated_at as permission_updated_at')
                ->get();

        foreach ($permissions as &$permission) {

            $permission->user_fullname = $permission->user_first_name . ' ' . $permission->user_last_name;
            $permission->permission_created_at = date('d-m-Y', $permission->permission_created_at);
            $permission->permission_updated_at = date('d-m-Y', $permission->permission_updated_at);
        }

        if (count($permissions) > 0) {

            return response()->json(['data' => $permissions]);
        } else {

            return response()->json(['status' => 'FAILED', 'message' => 'Invalid request.']);
        }
    }

}
