<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class PointController extends Controller {

    private $sitePath;

    public function __construct() {

        $this->sitePath = config('app.site_path');
    }

    public function add(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointTypes = DB::table('point_types')
                ->selectRaw('point_types.id as id, point_types.point_type_name as point_type_name, point_types.point_type_active as point_type_active')
                ->orderBy('point_type_name', 'ASC')
                ->get();

        return view('point/point-add', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'pointTypes' => $pointTypes]);
    }

    public function delete(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointIndex = $request->input('pointIndex');

        try {

            $rows = DB::table('points')
                    ->where(['id' => $pointIndex])
                    ->delete();

            if ($rows) {

                return response()->json(['status' => 'SUCCESS', 'message' => 'Point deleted successfully.']);
            } else {

                return response()->json(['status' => 'FAILED', 'message' => 'Point already deleted.']);
            }
        } catch (\Exception $e) {

            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
        }
    }

    public function save(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companyIndex = $this->userDetails[0]->company_id;
        $pointName = $request->input('pointName');
        $pointTypeIndex = $request->input('pointTypeIndex');
        $pointURL = $request->input('pointURL');
        $pointLatitude = $request->input('pointLatitude');
        $pointLongitude = $request->input('pointLongitude');

        isset($pointActive) ? $pointActive = "Yes" : $pointActive = "No";

        $validation = \Validator::make(
                        array(
                    'point_name' => $request->input('pointName'),
                    'point_url' => $request->input('pointURL'),
                    'point_latitude' => $request->input('pointLatitude'),
                    'point_longitude' => $request->input('pointLongitude'),
                        ), array(
                    'point_name' => 'required|string',
                    'point_url' => 'required|url',
                    'point_latitude' => 'required|numeric',
                    'point_longitude' => 'required|numeric'
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            try {

                $id = DB::table('points')->insertGetId(['company_id' => $companyIndex, 'point_name' => $pointName, 'point_type_id' => $pointTypeIndex, 'point_url' => $pointURL, 'point_latitude' => $pointLatitude, 'point_longitude' => $pointLongitude, 'point_active' => $pointActive, 'created_at' => time()]);

                if ($id) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'Point created successfully.']);
                } else {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function search(Request $request) {

        $pointLatitude = $request->input('latitude');
        $pointLongitude = $request->input('longitude');
        $pointRadius = $request->input('radius');

        $validation = \Validator::make(
                        array(
                    'point_latitude' => $request->input('latitude'),
                    'point_longitude' => $request->input('longitude'),
                    'point_radius' => $request->input('radius'),
                        ), array(
                    'point_latitude' => 'required|numeric',
                    'point_longitude' => 'required|numeric',
                    'point_radius' => 'required|numeric',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            $condition = 'points.id as id, points.point_name as point_name, point_types.point_type_name as point_type_name, points.point_url as point_url, points.point_latitude as point_latitude, points.point_longitude as point_longitude, points.point_active as point_active, points.created_at as point_created_at, points.updated_at as point_updated_at, ';
            $condition .= '(6371 * acos(cos(radians(' . $pointLatitude . ')) * cos(radians(point_latitude)) *          ';
            $condition .= 'cos(radians(point_longitude) - radians(' . $pointLongitude . ')) + sin(radians(              ';
            $condition .= $pointLatitude . ')) * sin(radians(point_latitude)))) as distance ';

            $points = DB::table('points')
                    ->join('companies', 'points.company_id', '=', 'companies.id')
                    ->join('point_types', 'points.point_type_id', '=', 'point_types.id')
                    ->selectRaw($condition)
                    ->get();

            $index = 0;

            foreach ($points as &$point) {

                if (floatval($point->distance) >= floatval($pointRadius)) {

                    unset($points[$index]);
                } else {
                    $point->point_created_at = date('d-m-Y', $point->point_created_at);
                    $point->point_updated_at = date('d-m-Y', $point->point_updated_at);
                }

                $index++;
            }

            return response()->json(['data' => $points]);
        }
    }

    public function update(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointIndex = $request->input('pointIndex');
        $companyIndex = $this->userDetails[0]->company_id;
        $pointName = $request->input('pointName');
        $pointTypeIndex = $request->input('pointTypeIndex');
        $pointURL = $request->input('pointURL');
        $pointLatitude = $request->input('pointLatitude');
        $pointLongitude = $request->input('pointLongitude');

        isset($pointActive) ? $pointActive = "Yes" : $pointActive = "No";

        $validation = \Validator::make(
                        array(
                    'point_name' => $request->input('pointName'),
                    'point_url' => $request->input('pointURL'),
                    'point_latitude' => $request->input('pointLatitude'),
                    'point_longitude' => $request->input('pointLongitude'),
                        ), array(
                    'point_name' => 'required|string',
                    'point_url' => 'required|url',
                    'point_latitude' => 'required|numeric',
                    'point_longitude' => 'required|numeric'
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            try {

                $status = DB::table('points')
                        ->where('id', $pointIndex)
                        ->update(['point_name' => $pointName, 'point_type_id' => $pointTypeIndex, 'point_url' => $pointURL, 'point_latitude' => $pointLatitude, 'point_longitude' => $pointLongitude, 'point_active' => $pointActive, 'updated_at' => time()]);

                if ($status == 1) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'Point updated successfully.']);
                } else {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function view(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $point = DB::table('points')
                ->join('companies', 'points.company_id', '=', 'companies.id')
                ->join('point_types', 'points.point_type_id', '=', 'point_types.id')
                ->selectRaw('points.id as id, points.point_name as point_name, points.point_type_id as point_type_id, point_types.point_type_name as point_type_name, points.point_url as point_url, points.point_latitude as point_latitude, points.point_longitude as point_longitude, points.point_active as point_active, points.created_at as point_created_at, points.updated_at as point_updated_at')
                ->where('points.id', $request->input('id'))
                ->get();

        $pointTypes = DB::table('point_types')
                ->selectRaw('point_types.id as id, point_types.point_type_name as point_type_name, point_types.point_type_active as point_type_active')
                ->orderBy('point_type_name', 'ASC')
                ->get();

        $point[0]->point_created_at = date('d-m-Y', $point[0]->point_created_at);
        $point[0]->point_updated_at = date('d-m-Y', $point[0]->point_updated_at);

        return view('point/point-details', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'pointTypes' => $pointTypes, 'point' => $point]);
    }

    public function viewAll(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $points = DB::table('points')
                ->join('companies', 'points.company_id', '=', 'companies.id')
                ->join('point_types', 'points.point_type_id', '=', 'point_types.id')
                ->selectRaw('points.id as id, points.point_name as point_name, point_types.point_type_name as point_type_name, points.point_url as point_url, points.point_latitude as point_latitude, points.point_longitude as point_longitude, points.point_active as point_active, points.created_at as point_created_at, points.updated_at as point_updated_at')
                ->get();

        foreach ($points as &$point) {

            $point->point_created_at = date('d-m-Y', $point->point_created_at);
            $point->point_updated_at = date('d-m-Y', $point->point_updated_at);
        }

        if (count($points) > 0) {

            return response()->json(['data' => $points]);
        } else {

            return response()->json(['status' => 'FAILED', 'message' => 'Invalid request.']);
        }
    }

}
