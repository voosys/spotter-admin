<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class PointTypeController extends Controller {

    private $sitePath;

    public function __construct() {

        $this->sitePath = config('app.site_path');
    }

    public function add(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        return view('point-type/point-type-add', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
    }

    public function delete(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointTypeIndex = $request->input('pointTypeIndex');

        try {

            $rows = DB::table('point_types')
                    ->where(['id' => $pointTypeIndex])
                    ->delete();

            if ($rows) {

                return response()->json(['status' => 'SUCCESS', 'message' => 'Point type deleted successfully.']);
            } else {

                return response()->json(['status' => 'FAILED', 'message' => 'Point type already deleted.']);
            }
        } catch (\Exception $e) {

            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
        }
    }

    public function save(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointTypeName = $request->input('pointTypeName');
        
        isset($pointTypeActive) ? $pointTypeActive = "Yes" : $pointTypeActive = "No";

        $validation = \Validator::make(
                        array(
                    'point_type_name' => $request->input('pointTypeName'),
                        ), array(
                    'point_type_name' => 'required|string',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            try {

                $id = DB::table('point_types')->insertGetId(['point_type_name' => $pointTypeName, 'point_type_active' => $pointTypeActive, 'created_at' => time()]);

                if ($id) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'Point type created successfully.']);
                } else {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function update(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointTypeIndex = $request->input('pointTypeIndex');
        $pointTypeName = $request->input('pointTypeName');
        $pointTypeActive = $request->input('pointTypeActive');
        
        isset($pointTypeActive) ? $pointTypeActive = "Yes" : $pointTypeActive = "No";

        $validation = \Validator::make(
                        array(
                    'point_type_name' => $request->input('pointTypeName'),                    
                        ), array(
                    'point_type_name' => 'required|string',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            try {

                $status = DB::table('point_types')
                        ->where('id', $pointTypeIndex)
                        ->update(['point_type_name' => $pointTypeName, 'point_type_active' => $pointTypeActive, 'updated_at' => time()]);

                if ($status == 1) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'Point type updated successfully.']);
                } else {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function view(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointTypes = DB::table('point_types')
                ->selectRaw('point_types.id as id, point_types.point_type_name as point_type_name, point_types.point_type_active as point_type_active, point_types.created_at as point_type_created_at, point_types.updated_at as point_type_updated_at')
                ->where('point_types.id', $request->input('id'))                
                ->get();

        $pointTypes[0]->point_type_created_at = date('d-m-Y', $pointTypes[0]->point_type_created_at);
        $pointTypes[0]->point_type_updated_at = date('d-m-Y', $pointTypes[0]->point_type_updated_at);

        return view('point-type/point-type-details', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'point_type' => $pointTypes]);
    }

    public function viewAll(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $pointTypes = DB::table('point_types')
                ->selectRaw('point_types.id as id, point_types.point_type_name as point_type_name, point_types.point_type_active as point_type_active, point_types.created_at as point_type_created_at, point_types.updated_at as point_type_updated_at')
                ->get();

        foreach ($pointTypes as &$pointType) {

            $pointType->point_type_created_at = date('d-m-Y', $pointType->point_type_created_at);
            $pointType->point_type_updated_at = date('d-m-Y', $pointType->point_type_updated_at);
        }

        if (count($pointTypes) > 0) {

            return response()->json(['data' => $pointTypes]);
        } else {

            return response()->json(['status' => 'FAILED', 'message' => 'Invalid request.']);
        }
    }

}
