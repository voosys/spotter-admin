<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller {

    private $sitePath;

    public function __construct() {

        $this->sitePath = config('app.site_path');
    }

    public function add(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companies = DB::table('companies')
                ->selectRaw('companies.id as id, companies.company_name as company_name, companies.company_active as company_active')
                ->orderBy('company_name', 'ASC')
                ->get();

        return view('user/user-add', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'companies' => $companies]);
    }

    public function changePassword(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        return view('user/change-password', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails]);
    }

    private function createUserPermission($companyId, $id, $userRole) {

        $read = "Yes";
        $write = "Yes";
        $delete = "Yes";

        switch ($userRole) {

            case 'Admin':

                $read = "Yes";
                $write = "Yes";
                $delete = "Yes";

                break;

            case 'Manager':

                $read = "Yes";
                $write = "Yes";
                $delete = "Yes";

                break;

            case 'Editor':

                $read = "Yes";
                $write = "Yes";
                $delete = "No";

                break;
        }

        $id = DB::table('permissions')->insertGetId(['company_id' => $companyId, 'user_id' => $id, 'permission_read' => $read, 'permission_write' => $write, 'permission_delete' => $delete, 'created_at' => time()]);
    }

    public function delete(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $userIndex = $request->input('userIndex');

        if ($this->userDetails[0]->id == $userIndex) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => ['Unable to delete the user as the user is currently active and logged in.']]);
        } else {

            try {

                $files = glob('./uploads/users/' . $userIndex . '/*');

                foreach ($files as $file) {

                    if (is_file($file)) {

                        unlink($file);
                    }
                }

                rmdir('./uploads/users/' . $userIndex);

                $rows = DB::table('users')
                        ->where(['id' => $userIndex])
                        ->delete();

                if ($rows) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'User deleted successfully.']);
                } else {

                    return response()->json(['status' => 'FAILED', 'message' => 'User already deleted.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function logout() {

        session()->flush();

        return redirect($this->sitePath);
    }

    public function profile(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $user = DB::table('users')
                ->selectRaw('users.id as id, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, users.user_email_address as user_email_address, users.user_phone_number as user_phone_number, users.user_profile_picture as user_profile_picture, users.user_role as user_role, users.user_active as user_active, users.user_last_logged_in as user_last_logged_in, users.created_at as user_created_at, users.updated_at as user_updated_at')
                ->where('users.id', $this->userDetails[0]->id)
                ->get();

        return view('user/profile', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'user' => $user]);
    }

    public function resetPassword(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $user = DB::table('users')
                ->where('id', $request->id)
                ->get();

        return view('user/user-reset-password', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'user' => $user]);
    }

    public function save(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $validation = \Validator::make(
                        array(
                    'user_first_name' => Input::get('userFirstName'),
                    'user_last_name' => Input::get('userLastName'),
                    'user_username' => Input::get('userName'),
                    'user_password' => Input::get('userPassword'),
                    'user_email_address' => Input::get('userEmailAddress'),
                    'user_phone_number' => Input::get('userPhoneNumber'),
                        ), array(
                    'user_first_name' => 'required|string',
                    'user_last_name' => 'required|string',
                    'user_username' => 'required|unique:users',
                    'user_password' => 'required|string',
                    'user_email_address' => 'required|email',
                    'user_phone_number' => 'required|string',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            $userFirstName = $request->input('userFirstName');
            $userLastName = $request->input('userLastName');
            $userName = $request->input('userName');
            $userPassword = md5($request->input('userPassword'));
            $userEmailAddress = $request->input('userEmailAddress');
            $userPhoneNumber = $request->input('userPhoneNumber');
            $userCompany = $request->input('userCompany');
            $userRole = $request->input('userRole');
            $userActive = $request->input('userActive');
            $userProfilePicture = NULL;

            isset($userActive) ? $userActive = "Yes" : $userActive = "No";

            if ($request->hasFile('userProfilePicture') && $request->file('userProfilePicture')->isValid()) {

                $extension = $request->file('userProfilePicture')->getClientOriginalExtension();

                $fileName = md5(time()) . '.' . $extension;

                try {

                    $id = DB::table('users')->insertGetId(['company_id' => $userCompany, 'user_first_name' => $userFirstName, 'user_last_name' => $userLastName, 'user_username' => $userName, 'user_password' => $userPassword, 'user_email_address' => $userEmailAddress, 'user_phone_number' => $userPhoneNumber, 'user_role' => $userRole, 'user_active' => $userActive, 'created_at' => time()]);

                    if ($id) {

                        $request->file('userProfilePicture')->move('./uploads/users/' . $id, $fileName);

                        $status = DB::table('users')
                                ->where('id', $id)
                                ->update(['user_profile_picture' => $fileName]);

                        if ($status == 1) {

                            $this->createUserPermission($userCompany, $id, $userRole);

                            return response()->json(['status' => 'SUCCESS', 'message' => 'User created successfully.']);
                        } else {

                            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                        }
                    } else {

                        return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                    }
                } catch (\Exception $e) {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            } else {

                try {

                    $id = DB::table('users')->insertGetId(['company_id' => $userCompany, 'user_first_name' => $userFirstName, 'user_last_name' => $userLastName, 'user_username' => $userName, 'user_password' => $userPassword, 'user_email_address' => $userEmailAddress, 'user_phone_number' => $userPhoneNumber, 'user_role' => $userRole, 'user_active' => $userActive, 'created_at' => time()]);

                    if ($id) {

                        $this->createUserPermission($userCompany, $id, $userRole);

                        return response()->json(['status' => 'SUCCESS', 'message' => 'User created successfully.']);
                    } else {

                        return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                    }
                } catch (\Exception $e) {

                    return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
                }
            }
        }
    }

    public function saveProfile(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $userIndex = $this->userDetails[0]->id;
        $userFirstName = $request->input('userFirstName');
        $userLastName = $request->input('userLastName');
        $userEmailAddress = $request->input('userEmailAddress');
        $userPhoneNumber = $request->input('userPhoneNumber');
        $userProfilePicture = NULL;

        $validation = \Validator::make(
                        array(
                    'user_first_name' => Input::get('userFirstName'),
                    'user_last_name' => Input::get('userLastName'),
                    'user_email_address' => Input::get('userEmailAddress'),
                    'user_phone_number' => Input::get('userPhoneNumber'),
                        ), array(
                    'user_first_name' => 'required|string',
                    'user_last_name' => 'required|string',
                    'user_email_address' => 'required|email',
                    'user_phone_number' => 'required|string',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            try {

                if ($request->hasFile('userProfilePicture') && $request->file('userProfilePicture')->isValid()) {

                    $files = glob('./uploads/users/' . $userIndex . '/*');

                    foreach ($files as $file) {

                        if (is_file($file)) {

                            unlink($file);
                        }
                    }

                    $extension = $request->file('userProfilePicture')->getClientOriginalExtension();

                    $fileName = md5(time()) . '.' . $extension;

                    $request->file('userProfilePicture')->move('./uploads/users/' . $userIndex, $fileName);

                    $status = DB::table('users')
                            ->where('id', $userIndex)
                            ->update(['user_first_name' => $userFirstName, 'user_last_name' => $userLastName, 'user_email_address' => $userEmailAddress, 'user_phone_number' => $userPhoneNumber, 'user_profile_picture' => $fileName, 'updated_at' => time()]);
                } else {

                    $status = DB::table('users')
                            ->where('id', $userIndex)
                            ->update(['user_first_name' => $userFirstName, 'user_last_name' => $userLastName, 'user_email_address' => $userEmailAddress, 'user_phone_number' => $userPhoneNumber, 'updated_at' => time()]);
                }

                if ($status == 1) {

                    $updatedUserDetails = DB::table('users')
                            ->join('permissions', 'permissions.user_id', '=', 'users.id')
                            ->selectRaw('users.id as id, users.company_id as company_id, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, users.user_email_address as user_email_address, users.user_phone_number as user_phone_number, users.user_profile_picture as user_profile_picture, users.user_role as user_role, users.user_active as user_active, permissions.permission_read, permissions.permission_write, permissions.permission_delete, users.user_last_logged_in as user_last_logged_in, users.created_at as user_created_at, users.updated_at as user_updated_at')
                            ->where('users.id', $userIndex)
                            ->get();

                    $this->updateUserDetails($updatedUserDetails);

                    session()->put('userDetails', $updatedUserDetails);

                    return response()->json(['status' => 'SUCCESS', 'message' => 'Profile details updated successfully.']);
                } else {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'Profile details is already update.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function signin() {

        return view('user/user-signin', ['sitePath' => $this->sitePath]);
    }

    public function verify(Request $request) {

        $userDetails = DB::table('users')
                ->join('permissions', 'permissions.user_id', '=', 'users.id')
                ->selectRaw('users.id as id, users.company_id as company_id, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, users.user_email_address as user_email_address, users.user_phone_number as user_phone_number, users.user_profile_picture as user_profile_picture, users.user_role as user_role, users.user_active as user_active, permissions.permission_read, permissions.permission_write, permissions.permission_delete, users.user_last_logged_in as user_last_logged_in, users.created_at as user_created_at, users.updated_at as user_updated_at')
                ->where('user_username', $request->userName)
                ->where('user_password', md5($request->password))
                ->get();
        
        
        if (count($userDetails) > 0) {

            $request->session()->put('userDetails', $userDetails);

            $userIndex = $userDetails[0]->id;

            $status = DB::table('users')
                    ->where('id', $userIndex)
                    ->update(['user_last_logged_in' => time()]);


            return response()->json(['status' => 'SUCCESS']);
        } else {

            return response()->json(['status' => 'FAILED', 'message' => 'Login failed. Please try again.']);
        }
    }

    public function update(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $userIndex = $request->input('userIndex');
        $userFirstName = $request->input('userFirstName');
        $userLastName = $request->input('userLastName');
        $userEmailAddress = $request->input('userEmailAddress');
        $userPhoneNumber = $request->input('userPhoneNumber');
        $userCompany = $request->input('userCompany');
        $userRole = $request->input('userRole');
        $userActive = $request->input('userActive');

        isset($userActive) ? $userActive = "Yes" : $userActive = "No";

        try {

            $status = DB::table('users')
                    ->where('id', $userIndex)
                    ->update(['user_first_name' => $userFirstName, 'user_last_name' => $userLastName, 'user_email_address' => $userEmailAddress, 'user_phone_number' => $userPhoneNumber, 'user_role' => $userRole, 'user_active' => $userActive, 'updated_at' => time()]);

            if ($status == 1) {

                return response()->json(['status' => 'SUCCESS', 'message' => 'User details updated successfully.']);
            } else {

                return response()->json(['status' => 'SUCCESS', 'message' => 'User details is already update.']);
            }
        } catch (\Exception $e) {

            return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
        }
    }

    public function updatePassword(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->sessionExists()) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $validation = \Validator::make(
                        array(
                    'user_new_password' => Input::get('userNewPassword'),
                    'user_confirm_password' => Input::get('userConfirmNewPassword'),
                        ), array(
                    'user_new_password' => 'required|string',
                    'user_confirm_password' => 'required|string',
                        )
        );

        if ($validation->fails()) {

            return response()->json(['status' => 'VALIDATION_FAILED', 'messages' => $validation->messages()->all()]);
        } else {

            $userId = $request->input('userId');
            $userNewPassword = md5($request->input('userNewPassword'));
            $userConfirmNewPassword = md5($request->input('userConfirmNewPassword'));

            try {

                $status = DB::table('users')
                        ->where('id', $userId)
                        ->update(['user_password' => $userNewPassword, 'updated_at' => time()]);

                if ($status == 1) {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'User password updated successfully.']);
                } else {

                    return response()->json(['status' => 'SUCCESS', 'message' => 'User password is already update.']);
                }
            } catch (\Exception $e) {

                return response()->json(['status' => 'FAILED', 'message' => 'Sorry an error occurred.']);
            }
        }
    }

    public function view(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $companies = DB::table('companies')
                ->selectRaw('companies.id as id, companies.company_name as company_name, companies.company_active as company_active')
                ->orderBy('company_name', 'ASC')
                ->get();

        $user = DB::table('users')
                ->selectRaw('users.id as id, users.company_id as company_id, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, users.user_email_address as user_email_address, users.user_phone_number as user_phone_number, users.user_role as user_role, users.user_active as user_active, users.user_last_logged_in as user_last_logged_in, users.user_active as user_active, users.created_at as user_created_at, users.updated_at as user_updated_at')
                ->where('id', $request->id)
                ->get();

        $user[0]->user_created_at = date('d-m-Y', $user[0]->user_created_at);
        $user[0]->user_updated_at = date('d-m-Y', $user[0]->user_updated_at);

        return view('user/user-details', ['sitePath' => $this->sitePath, 'userDetails' => $this->userDetails, 'user' => $user, 'companies' => $companies]);
    }

    public function viewAll(Request $request) {

        $this->userDetails = session()->get('userDetails');

        if (!$this->userDetails) {

            session()->flush();

            return redirect($this->sitePath);
        }

        $users = DB::table('users')
                ->selectRaw('users.id as id, users.user_first_name as user_first_name, users.user_last_name as user_last_name, users.user_username as user_username, users.user_role as user_role, users.user_active as user_active, users.created_at as user_created_at, users.updated_at as user_updated_at')
                ->get();

        foreach ($users as &$user) {

            $user->user_created_at = date('d-m-Y', $user->user_created_at);
            $user->user_updated_at = date('d-m-Y', $user->user_updated_at);
        }

        if (count($users) > 0) {

            return response()->json(['data' => $users]);
        } else {

            return response()->json(['status' => 'FAILED', 'message' => 'Invalid request.']);
        }
    }

}
