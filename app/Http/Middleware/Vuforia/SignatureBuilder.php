<?php

namespace App\Http\Middleware\Vuforia;

class SignatureBuilder {
		
	private $contentType = '';
	private $hexDigest   = 'd41d8cd98f00b204e9800998ecf8427e';
	
	public function tmsSignature($request, $secret_key) {
		
		$method         = $request->getMethod();
		$requestHeaders = $request->getHeaders();
		$dateValue      = $requestHeaders['date'];
		$requestPath    = $request->getURL()->getPath();
		
		if (isset($requestHeaders['content-type'])) {
		
			$this->contentType = $requestHeaders['content-type'];
			
		}
	
		if ($method == 'GET' || $method == 'DELETE') {
		
		
		} else if ($method == 'POST' || $method == 'PUT') {
			
			$this->hexDigest = md5($request->getBody(), false);
			
		} else {
		
			print("ERROR: Invalid content type passed to Sig Builder");
			
		}
		
		$toDigest = $method . "\n" . $this->hexDigest . "\n" . $this->contentType . "\n" . $dateValue . "\n" . $requestPath ;

		$shaHashed = "";
		
		try {
			
			$shaHashed = $this->hexToBase64(hash_hmac("sha1", $toDigest, $secret_key));
			
		} catch (Exception $e) {
			
			$e->getMessage();
			
		}

		return $shaHashed;
		
	}
		
	private function hexToBase64($hex) {
	
		$return = "";
	
		foreach (str_split($hex, 2) as $pair) {
	
			$return .= chr(hexdec($pair));
	
		}
	
		return base64_encode($return);
		
	}

}