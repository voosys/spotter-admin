<?php

namespace App\Http\Middleware\Vuforia;

ini_set("include_path", '/home/aa42/php:' . ini_get("include_path") );

require_once 'HTTP/Request2.php';
require_once 'SignatureBuilder.php';

class VWS {

    private $access_key = '';
    private $secret_key = '';
    private $url = '';
    private $requestPath = "/targets";
    private $request;
    private $response;
    private $jsonRequestObject;
    private $targetId = "";
    private $targetName = "";
    private $imageLocation = "";
    private $imageWidth = 0;

    private function getImageAsBase64() {

        $file = file_get_contents($this->imageLocation);

        if ($file) {

            $file = base64_encode($file);
        }

        return $file;
    }

    private function setHeaders($mode) {

        $sb = new SignatureBuilder();
        $date = new DateTime("now", new DateTimeZone("GMT"));

        $this->request->setHeader('Date', $date->format("D, d M Y H:i:s") . " GMT");

        if ($mode == "create") {

            $this->request->setHeader("Content-Type", "application/json");
        } else if ($mode == "update") {

            $this->request->setHeader("Content-Type", "application/json");
        }

        $this->request->setHeader("Authorization", "VWS " . $this->access_key . ":" . $sb->tmsSignature($this->request, $this->secret_key));
    }

    public function __construct() {
        
    }

    public function config($accessKey, $secretKey, $url) {

        $this->access_key = $accessKey;
        $this->secret_key = $secretKey;
        $this->url = $url;
    }

    public function summary() {

        $this->requestPath = "/summary";
        $this->request = new HTTP_Request2();

        $this->request->setMethod(HTTP_Request2::METHOD_GET);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("summary");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = $response->getBody();
            } else {

                $this->response = $response->getBody();
            }
        } catch (HTTP_Request2_Exception $e) {

            echo 'Error: ' . $e->getMessage();
        }
    }

    public function view($targetId) {

        $this->request = new HTTP_Request2();
        $this->targetId = $targetId;
        $this->requestPath = $this->requestPath . '/' . $this->targetId;

        $this->request->setMethod(HTTP_Request2::METHOD_GET);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("view");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                //print_r($this->response->target_record);

                return $this->response->target_record;
            } else {

                $this->response = json_decode($response->getBody());

                return null;
            }
        } catch (HTTP_Request2_Exception $e) {

            return null;
        }
    }

    public function viewAll() {

        $this->request = new HTTP_Request2();

        $this->request->setMethod(HTTP_Request2::METHOD_GET);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("view-all");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                print_r($this->response);

                return 'SUCCESS';
            } else {

                $this->response = json_decode($response->getBody());

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

    public function create($targetName, $imageLocation, $targetMetaData) {

        $this->request = new HTTP_Request2();
        $this->targetName = $targetName;
        $this->imageLocation = $imageLocation;
        $imageDetails = getimagesize($imageLocation);
        $this->imageWidth = $imageDetails[0];
        $this->jsonRequestObject = json_encode(array('width' => $this->imageWidth, 'name' => $this->targetName, 'image' => $this->getImageAsBase64(), 'application_metadata' => base64_encode($targetMetaData), 'active_flag' => 1));

        $this->request->setMethod(HTTP_Request2::METHOD_POST);
        $this->request->setBody($this->jsonRequestObject);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("create");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus() || 201 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                if ($this->response->target_id != "") {

                    return $this->response->target_id;
                }
            } else {

                $this->response = $response->getBody();

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

    public function update($targetId, $targetMetaData) {

        $this->request = new HTTP_Request2();
        $this->targetId = $targetId;
        $this->requestPath = $this->requestPath . '/' . $this->targetId;
        $this->jsonRequestObject = json_encode(array('application_metadata' => base64_encode($targetMetaData)));

        $this->request->setMethod(HTTP_Request2::METHOD_PUT);
        $this->request->setBody($this->jsonRequestObject);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("update");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                return $this->response;
            } else {

                $this->response = $response->getBody();

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

    public function updateImage($targetId, $imageLocation) {

        $this->request = new HTTP_Request2();
        $this->targetId = $targetId;
        $this->imageLocation = $imageLocation;
        $imageDetails = getimagesize($imageLocation);
        $this->imageWidth = $imageDetails[0];
        $this->requestPath = $this->requestPath . '/' . $this->targetId;
        $this->jsonRequestObject = json_encode(array('width' => $this->imageWidth, 'image' => $this->getImageAsBase64(), 'active_flag' => 1));

        $this->request->setMethod(HTTP_Request2::METHOD_PUT);
        $this->request->setBody($this->jsonRequestObject);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("update");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                return $this->response;
            } else {

                $this->response = $response->getBody();

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

    public function delete($targetId) {

        $this->request = new HTTP_Request2();
        $this->targetId = $targetId;

        $this->request->setMethod(HTTP_Request2::METHOD_DELETE);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath . '/' . $this->targetId);
        $this->setHeaders("delete");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus() || 201 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                if ($this->response->result_code == 'Success') {

                    return 'SUCCESS';
                }
            } else {

                $this->response = $response->getBody();

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

    public function activate($targetId) {

        $this->request = new HTTP_Request2();
        $this->targetId = $targetId;
        $this->requestPath = $this->requestPath . '/' . $this->targetId;
        $this->jsonRequestObject = json_encode(array('active_flag' => true));

        $this->request->setMethod(HTTP_Request2::METHOD_PUT);
        $this->request->setBody($this->jsonRequestObject);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("update");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                return $this->response;
            } else {

                $this->response = $response->getBody();

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

    public function deactivate($targetId) {

        $this->request = new HTTP_Request2();
        $this->targetId = $targetId;
        $this->requestPath = $this->requestPath . '/' . $this->targetId;
        $this->jsonRequestObject = json_encode(array('active_flag' => false));

        $this->request->setMethod(HTTP_Request2::METHOD_PUT);
        $this->request->setBody($this->jsonRequestObject);
        $this->request->setConfig(array('ssl_verify_peer' => false));
        $this->request->setURL($this->url . $this->requestPath);
        $this->setHeaders("update");

        try {

            $response = $this->request->send();

            if (200 == $response->getStatus()) {

                $this->response = json_decode($response->getBody());

                return $this->response;
            } else {

                $this->response = $response->getBody();

                return 'FAILED';
            }
        } catch (HTTP_Request2_Exception $e) {

            return 'FAILED';
        }
    }

}

$v = new VWS();	
$v->summary();
var_dump($v);exit;

//echo 'Result code is: ' . $v->create("Penguins", 'Pengiuns.jpg', "Demo Meta Data");
//echo 'Result code is: ' . $v->create("Eagle", 'Eagle.jpg', "Demo Meta Data");
//echo 'Result code is: ' . $v->delete('9fffb53248204fd7ab03864166e4e498');
//var_dump($v->view('15b4a2d5404343bebd08919d9a654193'));
//$v->update('15b4a2d5404343bebd08919d9a654193', "Updated meta data");
//echo 'Result code is: ' . $v->viewAll();