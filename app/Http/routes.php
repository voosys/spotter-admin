<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'UserController@signin');
    Route::get('/signin', 'UserController@signin');
    Route::get('/admin', 'UserController@signin');
    Route::get('/companies', 'AppController@companies');
    Route::get('/points', 'AppController@points');
	Route::post('/points', 'AppController@points');
    Route::get('/point-types', 'AppController@pointTypes');
    Route::get('/permissions', 'AppController@permissions');
    Route::get('/track', 'AppController@track');
    Route::get('/users', 'AppController@users');
    
    Route::get('company/add', 'CompanyController@add');
    Route::post('company/delete', 'CompanyController@delete');
    Route::post('company/save', 'CompanyController@save');
    Route::post('company/update', 'CompanyController@update');
    Route::get('company/view', 'CompanyController@view');
    Route::get('company/view-all', 'CompanyController@viewAll');

    Route::get('point/add', 'PointController@add');
    Route::post('point/delete', 'PointController@delete');
    Route::post('point/save', 'PointController@save');
    Route::get('point/search', 'PointController@search');
    Route::post('point/update', 'PointController@update');
    Route::get('point/view', 'PointController@view');
    Route::get('point/view-all', 'PointController@viewAll');
    
    Route::get('point-type/add', 'PointTypeController@add');
    Route::post('point-type/delete', 'PointTypeController@delete');
    Route::post('point-type/save', 'PointTypeController@save');
    Route::post('point-type/update', 'PointTypeController@update');
    Route::get('point-type/view', 'PointTypeController@view');
    Route::get('point-type/view-all', 'PointTypeController@viewAll');

    Route::get('permission/add', 'PermissionController@add');
    Route::post('permission/delete', 'PermissionController@delete');
    Route::post('permission/save', 'PermissionController@save');
    Route::post('permission/update', 'PermissionController@update');
    Route::get('permission/view', 'PermissionController@view');
    Route::get('permission/view-all', 'PermissionController@viewAll');

    Route::get('user/add', 'UserController@add');
    Route::post('user/delete', 'UserController@delete');
    Route::post('user/save', 'UserController@save');
    Route::get('user/view', 'UserController@view');
    Route::get('user/view-all', 'UserController@viewAll');
    Route::post('user/verify', 'UserController@verify');
    Route::get('user/signup', 'UserController@signup');
    Route::get('user/profile', 'UserController@profile');
    Route::post('user/save-profile', 'UserController@saveProfile');
    Route::get('user/logout', 'UserController@logout');
    Route::get('user/change-password', 'UserController@changePassword');
    Route::get('user/reset-password', 'UserController@resetPassword');
    Route::post('user/update', 'UserController@update');
    Route::post('user/update-password', 'UserController@updatePassword');
    
});