var companyForm = $("#companyForm");

companyForm.on('submit', function (e) {

    var formData = new FormData($(this)[0]);
    
    showOverlay();
    
    $.ajax({
        type: "POST",
        url: sitePath + "company/update",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data && data.status == "SUCCESS") {
                
                hideOverlay();
                toastr.success(data.message);

                window.location.href = sitePath + "companies";

            } else if (data && data.status == "VALIDATION_FAILED") {
                
                hideOverlay();
                data.messages.forEach(function (message) {

                    toastr.error(message);

                });

            } else if (data && data.status == "FAILED") {
                
                hideOverlay();
                toastr.error(data.message);

            }
        }
    });

    return false;
});

function updateCompany(e) {

    companyForm.submit();

}

function deleteCompany(e) {

    var companyForm = $("#companyForm");

    $.Zebra_Dialog(
            'Do you want to delete this company?',
            {title: 'Augment Campus',
                modal: true,
                show_close_button: false,
                type: 'question',
                'buttons': [
                    {caption: 'Yes', callback: function () {

                            showOverlay();

                            $.ajax({
                                type: "POST",
                                url: sitePath + "company/delete",
                                data: companyForm.serialize(),
                                success: function (data) {

                                    if (data.status == "SUCCESS") {

                                        hideOverlay();
                                        toastr.success(data.message);

                                        window.location.href = sitePath + "companies";

                                    } else if (data && data.status == "VALIDATION_FAILED") {

                                        hideOverlay();
                                        data.messages.forEach(function (message) {

                                            toastr.error(message);

                                        });

                                    } else if (data && data.status == "FAILED") {

                                        hideOverlay();
                                        toastr.error(data.message);

                                    }

                                }

                            });
                        }},
                    {caption: 'No', callback: function () {

                        }},
                ]
            }
    );

}