var companiesTable;

$(document).ready(function () {

    companiesTable = $('#companiesTable').DataTable({
        select: true,
        "ajax": sitePath + 'company/view-all', "columns": [
            {"data": "id"},
            {"data": "company_name"},
            {"data": "company_active"},
            {"data": "company_created_at"},
            {"data": "company_updated_at"}
        ],
        deferRender: true,
        stateSave: true
    });

    companiesTable.getSelectedRow = function () {

        return companiesTable.rows('.selected').data()[0];

    };

});

function newCompany() {

    window.location.href = 'company/add';

}

function openCompany(e) {

    var row = companiesTable.getSelectedRow();

    if (row && row.id) {

        window.location.href = sitePath + 'company/view?id=' + row.id;

    }

}

function deleteCompany(e) {

    var companiesForm = $("#companiesForm");
    var id = companiesTable.getSelectedRow().id;

    if (id != null) {

        $.Zebra_Dialog(
                'Do you want to delete this company?',
                {title: 'ID8 Hub',
                    modal: true,
                    show_close_button: false,
                    type: 'question',
                    'buttons': [
                        {caption: 'Yes', callback: function () {

                                $("#companyIndex")[0].value = id;

                                showOverlay();

                                $.ajax({
                                    type: "POST",
                                    url: sitePath + "company/delete",
                                    data: companiesForm.serialize(),
                                    success: function (data) {

                                        if (data.status == "SUCCESS") {

                                            hideOverlay();
                                            companiesTable.rows('.selected').remove().draw();

                                        } else if (data && data.status == "VALIDATION_FAILED") {

                                            hideOverlay();
                                            data.messages.forEach(function (message) {

                                                toastr.error(message);

                                            });

                                        } else if (data && data.status == "FAILED") {

                                            hideOverlay();
                                            toastr.error(data.message);

                                        }

                                    }

                                });
                            }},
                        {caption: 'No', callback: function () {

                            }},
                    ]
                }
        );

    }

}