var permissionsTable;

$(document).ready(function () {

    permissionsTable = $('#permissionsTable').DataTable({
        select: true,
        "ajax": sitePath + 'permission/view-all', "columns": [
            {"data": "id"},
            {"data": "company_name"},
            {"data": "user_fullname"},
            {"data": "permission_read"},
            {"data": "permission_write"},
            {"data": "permission_delete"},
            {"data": "permission_created_at"},
            {"data": "permission_updated_at"}
        ],
        deferRender: true,
        stateSave: true
    });

    permissionsTable.getSelectedRow = function () {

        return permissionsTable.rows('.selected').data()[0];

    }

});

function deletePermission(e) {

    var permissionsForm = $("#permissionsForm");
    var id = permissionsTable.getSelectedRow().id;

    if (id != null) {

        $.Zebra_Dialog(
                'Do you want to delete this permission?',
                {title: 'Augment Campus',
                    modal: true,
                    show_close_button: false,
                    type: 'question',
                    'buttons': [
                        {caption: 'Yes', callback: function () {

                                $("#permissionIndex")[0].value = id;

                                showOverlay();

                                $.ajax({
                                    type: "POST",
                                    url: sitePath + "permission/delete",
                                    data: permissionsForm.serialize(),
                                    success: function (data) {

                                        if (data.status == "SUCCESS") {

                                            hideOverlay();
                                            permissionsTable.rows('.selected').remove().draw();

                                        } else if (data && data.status == "VALIDATION_FAILED") {

                                            hideOverlay();
                                            data.messages.forEach(function (message) {

                                                toastr.error(message);

                                            });

                                        } else if (data && data.status == "FAILED") {

                                            hideOverlay();
                                            toastr.error(data.message);

                                        }

                                    }

                                });
                            }},
                        {caption: 'No', callback: function () {

                            }},
                    ]
                }
        );

    }

}

function newPermission() {

    window.location.href = 'permission/add';

}

function openPermission(e) {

    var row = permissionsTable.getSelectedRow();

    if (row && row.id) {

        window.location.href = sitePath + 'permission/view?id=' + row.id;

    }

}