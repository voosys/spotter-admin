var newPointForm = $("#newPointForm");

newPointForm.on('submit', function (e) {

    var formData = new FormData($(this)[0]);
    
    showOverlay();
    
    $.ajax({
        type: "POST",
        url: sitePath + "point/save",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data && data.status == "SUCCESS") {
                
                hideOverlay();
                toastr.success(data.message);

                window.location.href = sitePath + "points";

            } else if (data && data.status == "VALIDATION_FAILED") {
                
                hideOverlay();
                data.messages.forEach(function (message) {

                    toastr.error(message);

                });

            } else if (data && data.status == "FAILED") {
                
                hideOverlay();
                toastr.error(data.message);

            }
        }
    });

    return false;
});

function savePoint(e) {

    newPointForm.submit();

}