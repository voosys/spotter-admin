var pointForm = $("#pointForm");

pointForm.on('submit', function (e) {

    var formData = new FormData($(this)[0]);

    showOverlay();

    $.ajax({
        type: "POST",
        url: sitePath + "point/update",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data && data.status == "SUCCESS") {

                hideOverlay();
                toastr.success(data.message);

                window.location.href = sitePath + "points";

            } else if (data && data.status == "VALIDATION_FAILED") {

                hideOverlay();
                data.messages.forEach(function (message) {

                    toastr.error(message);

                });

            } else if (data && data.status == "FAILED") {

                hideOverlay();
                toastr.error(data.message);

            }
        }
    });

    return false;
});

function updatePoint(e) {

    pointForm.submit();

}

function deletePoint(e) {

    $.Zebra_Dialog(
            'Do you want to delete this point?',
            {title: 'Studio',
                modal: true,
                show_close_button: false,
                type: 'question',
                'buttons': [
                    {caption: 'Yes', callback: function () {

                            showOverlay();

                            $.ajax({
                                type: "POST",
                                url: sitePath + "point/delete",
                                data: pointForm.serialize(),
                                success: function (data) {

                                    if (data.status == "SUCCESS") {

                                        hideOverlay();
                                        toastr.success(data.message);

                                        window.location.href = sitePath + "points";

                                    } else if (data && data.status == "VALIDATION_FAILED") {

                                        hideOverlay();
                                        data.messages.forEach(function (message) {

                                            toastr.error(message);

                                        });

                                    } else if (data && data.status == "FAILED") {

                                        hideOverlay();
                                        toastr.error(data.message);

                                    }

                                }

                            });
                        }},
                    {caption: 'No', callback: function () {

                        }},
                ]
            }
    );

}