var pointTypesTable;

$(document).ready(function () {

    pointTypesTable = $('#pointTypesTable').DataTable({
        select: true,
        "ajax": sitePath + 'point-type/view-all', "columns": [
            {"data": "id"},
            {"data": "point_type_name"},
            {"data": "point_type_active"},
            {"data": "point_type_created_at"},
            {"data": "point_type_updated_at"}
        ],
        deferRender: true,
        stateSave: true
    });

    pointTypesTable.getSelectedRow = function () {

        return pointTypesTable.rows('.selected').data()[0];

    }

});

function newPointType() {

    window.location.href = 'point-type/add';

}

function openPointType(e) {

    var row = pointTypesTable.getSelectedRow();

    if (row && row.id) {

        window.location.href = sitePath + 'point-type/view?id=' + row.id;

    }

}

function deletePointType(e) {

    var pointTypesForm = $("#pointTypesForm");
    var id = pointTypesTable.getSelectedRow().id;

    if (id != null) {

        $.Zebra_Dialog(
                'Do you want to delete this point type?',
                {title: 'Studio',
                    modal: true,
                    show_close_button: false,
                    type: 'question',
                    'buttons': [
                        {caption: 'Yes', callback: function () {

                                $("#pointTypeIndex")[0].value = id;

                                showOverlay();

                                $.ajax({
                                    type: "POST",
                                    url: sitePath + "point-type/delete",
                                    data: pointTypesForm.serialize(),
                                    success: function (data) {

                                        if (data.status == "SUCCESS") {

                                            hideOverlay();
                                            pointTypesTable.rows('.selected').remove().draw();

                                        } else if (data && data.status == "VALIDATION_FAILED") {

                                            hideOverlay();
                                            data.messages.forEach(function (message) {

                                                toastr.error(message);

                                            });

                                        } else if (data && data.status == "FAILED") {

                                            hideOverlay();
                                            toastr.error(data.message);

                                        }

                                    }

                                });
                            }},
                        {caption: 'No', callback: function () {

                            }},
                    ]
                }
        );

    }

}