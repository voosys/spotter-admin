var pointsTable;

$(document).ready(function () {

    pointsTable = $('#pointsTable').DataTable({
        select: true,
        "ajax": sitePath + 'point/view-all', "columns": [
            {"data": "id"},
            {"data": "point_name"},
            {"data": "point_type_name"},
            {"data": "point_latitude"},
            {"data": "point_longitude"},
            {"data": "point_created_at"},
            {"data": "point_updated_at"}
        ],
        deferRender: true,
        stateSave: true
    });

    pointsTable.getSelectedRow = function () {

        return pointsTable.rows('.selected').data()[0];

    }

});

function newPoint() {

    window.location.href = 'point/add';

}

function openPoint(e) {

    var row = pointsTable.getSelectedRow();

    if (row && row.id) {

        window.location.href = sitePath + 'point/view?id=' + row.id;

    }

}

function deletePoint(e) {

    var pointsForm = $("#pointsForm");
    var id = pointsTable.getSelectedRow().id;

    if (id != null) {

        $.Zebra_Dialog(
                'Do you want to delete this point?',
                {title: 'Studio',
                    modal: true,
                    show_close_button: false,
                    type: 'question',
                    'buttons': [
                        {caption: 'Yes', callback: function () {

                                $("#pointIndex")[0].value = id;

                                showOverlay();

                                $.ajax({
                                    type: "POST",
                                    url: sitePath + "point/delete",
                                    data: pointsForm.serialize(),
                                    success: function (data) {

                                        if (data.status == "SUCCESS") {

                                            hideOverlay();
                                            pointsTable.rows('.selected').remove().draw();

                                        } else if (data && data.status == "VALIDATION_FAILED") {

                                            hideOverlay();
                                            data.messages.forEach(function (message) {

                                                toastr.error(message);

                                            });

                                        } else if (data && data.status == "FAILED") {

                                            hideOverlay();
                                            toastr.error(data.message);

                                        }

                                    }

                                });
                            }},
                        {caption: 'No', callback: function () {

                            }},
                    ]
                }
        );

    }

}