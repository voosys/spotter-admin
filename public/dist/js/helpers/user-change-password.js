function changeUserPassword(e) {

    var changeUserPasswordForm = $("#changeUserPasswordForm");
    
    showOverlay();
    
    $.ajax({
        type: "POST",
        url: sitePath + "user/update-password",
        data: changeUserPasswordForm.serialize(),
        success: function (data) {

            if (data.status == "SUCCESS") {
                
                hideOverlay();
                toastr.success(data.message);

            } else if (data && data.status == "VALIDATION_FAILED") {
                
                hideOverlay();
                data.messages.forEach(function (message) {

                    toastr.error(message);

                });
            } else {
                
                hideOverlay();
                toastr.error(data.message);

            }
        }

    });

}