var userForm = $("#userForm");

userForm.on('submit', function (e) {

    var formData = new FormData($(this)[0]);

    showOverlay();

    $.ajax({
        type: "POST",
        url: sitePath + "user/update",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {

            if (data && data.status == "SUCCESS") {

                hideOverlay();
                toastr.success(data.message);

                window.location.href = sitePath + "users";

            } else if (data && data.status == "VALIDATION_FAILED") {

                hideOverlay();
                data.messages.forEach(function (message) {

                    toastr.error(message);

                });

            } else if (data && data.status == "FAILED") {

                hideOverlay();
                toastr.error(data.message);

            }
        }
    });

    return false;
});

function updateUser(e) {

    userForm.submit();

}

function deleteUser(e) {

    var userForm = $("#userForm");

    $.Zebra_Dialog(
            'Do you want to delete this user?',
            {title: 'ID8 Hub',
                modal: true,
                show_close_button: false,
                type: 'question',
                'buttons': [
                    {caption: 'Yes', callback: function () {

                            showOverlay();

                            $.ajax({
                                type: "POST",
                                url: sitePath + "user/delete",
                                data: userForm.serialize(),
                                success: function (data) {

                                    if (data.status == "SUCCESS") {

                                        hideOverlay();
                                        toastr.success(data.message);

                                        window.location.href = sitePath + "users";

                                    } else if (data && data.status == "VALIDATION_FAILED") {

                                        hideOverlay();
                                        data.messages.forEach(function (message) {

                                            toastr.error(message);

                                        });

                                    } else if (data && data.status == "FAILED") {

                                        hideOverlay();
                                        toastr.error(data.message);

                                    }

                                }

                            });
                        }},
                    {caption: 'No', callback: function () {

                        }},
                ]
            }
    );

}