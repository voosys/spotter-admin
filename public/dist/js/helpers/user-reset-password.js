function resetUserPassword(e) {

    var resetUserPasswordForm = $("#resetUserPasswordForm");
    
    showOverlay();
    
    $.ajax({
        type: "POST",
        url: sitePath + "user/update-password",
        data: resetUserPasswordForm.serialize(),
        success: function (data) {

            if (data.status == "SUCCESS") {
                
                hideOverlay();
                toastr.success(data.message);
                
                window.location.href = sitePath + "users";

            } else {
                
                hideOverlay();
                toastr.error(data.message);

            }
        }

    });

}