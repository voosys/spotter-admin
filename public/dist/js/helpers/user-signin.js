var signinForm = $('#signinForm');

signinForm.on('submit', function (e) {

    $.ajax({
        type: "POST",
        data: signinForm.serialize(),
        url: sitePath + "user/verify",
    }).done(function (response) {

        if (response.status == "SUCCESS") {

            window.location.href = sitePath + "points";

        } else if (response.status == "FAILED") {

            alert(response.message);

        }

    });

    return false;

});

function signin(e) {

    signinForm.submit();

}