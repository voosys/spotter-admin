var usersTable;

$(document).ready(function () {

    usersTable = $('#usersTable').DataTable({
        select: true,
        "ajax": sitePath + 'user/view-all', "columns": [
            {"data": "id"},
            {"data": "user_first_name"},
            {"data": "user_last_name"},
            {"data": "user_username"},
            {"data": "user_role"},
            {"data": "user_active"},
            {"data": "user_created_at"},
            {"data": "user_updated_at"}
        ],
        deferRender: true,
        stateSave: true
    });

    usersTable.getSelectedRow = function () {

        return usersTable.rows('.selected').data()[0];

    }

});

function newUser() {

    window.location.href = 'user/add';

}

function openUser(e) {

    var row = usersTable.getSelectedRow();

    if (row && row.id) {

        window.location.href = sitePath + 'user/view?id=' + row.id;

    }

}

function deleteUser(e) {

    var usersForm = $("#usersForm");
    var id = usersTable.getSelectedRow().id;

    if (id != null) {

        $.Zebra_Dialog(
                'Do you want to delete this user?',
                {title: 'Augment Campus',
                    modal: true,
                    show_close_button: false,
                    type: 'question',
                    'buttons': [
                        {caption: 'Yes', callback: function () {

                                $("#userIndex")[0].value = id;

                                showOverlay();

                                $.ajax({
                                    type: "POST",
                                    url: sitePath + "user/delete",
                                    data: usersForm.serialize(),
                                    success: function (data) {

                                        if (data.status == "SUCCESS") {

                                            hideOverlay();
                                            usersTable.rows('.selected').remove().draw();

                                        } else if (data && data.status == "VALIDATION_FAILED") {

                                            hideOverlay();
                                            data.messages.forEach(function (message) {

                                                toastr.error(message);

                                            });

                                        } else if (data && data.status == "FAILED") {

                                            hideOverlay();
                                            toastr.error(data.message);

                                        }

                                    }

                                });
                            }},
                        {caption: 'No', callback: function () {

                            }},
                    ]
                }
        );

    }

}