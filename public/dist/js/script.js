function showOverlay() {

    $.LoadingOverlay("show",
            {
                color: "rgba(255, 255, 255, .75)",
                maxSize: 50,
                image: "",
                fontawesome: "fa fa-circle-o-notch fa-spin icon-white",
                zIndex: 1000
            }
    );
}

function hideOverlay() {

    $.LoadingOverlay("hide");
}


jQuery(document).ready(function () {

    "use strict";

    Core.init();
    Ajax.init();

    $('.dashboard-widget-tray .btn:first-child').on('click', function () {
        $('#widget-dropdown').slideToggle('fast');
    });
});