<?php

return [
    'already_update' => 'Record is already update...',    
    'delete_error' => 'Sorry, an error occurred while deleting the record...',
    'delete_success' => 'Record deleted successfully...',
    'password__and_confirm_password_should_match' => 'Password and confirm password should match',
    'permission_already_exists' => 'The specified permission already exists...',
    'save_error' => 'Sorry, an error occurred while saving the record...',
    'save_success' => 'Record saved successfully...',
    'update_error' => 'Sorry, an error occurred while updating the record...',
    'update_password_error' => 'Sorry, an error occurred while updating the password...',
    'update_password_failed' => 'The specified old password is invalid...',
    'update_password_success' => 'The password has been updated successfully...',
    'update_success' => 'Record updated successfully...',
];
