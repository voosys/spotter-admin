@extends('layout')

@section('page-title')

<title>New company | Companies | Spotter</title>

@endsection

@section('content')

<section class="content-header">
    <h1>Company details</h1>
</section>
<section class="content">				
    <div class="box">
        <div class="box-body">	
            <div class="row">
                @if ($userDetails[0]->permission_write == "Yes")
                <div class="col-md-1">
                    <div onclick="saveCompany();" class="btn btn-block btn-success" title="Save">
                        <span class="ion ion-checkmark"/>						
                    </div>
                </div>
                @endif
                <div class="col-md-1">
                    <a class="btn btn-block btn-primary" href="{{$sitePath}}companies" title="Back">
                        <span class="ion ion-android-arrow-back"></span>
                    </a>
                </div>				
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">			
            <form name="newCompanyForm" id="newCompanyForm" role="form">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Company name</label>
                            <input type="text" name="companyName" id="companyName" class="form-control" placeholder="Enter company name">
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Company website URL</label>
                            <input type="text" name="companyWebsiteURL" id="companyWebsiteURL" class="form-control" placeholder="Enter webiste URL">
                        </div>                
                    </div>
                </div>             
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Email address</label>
                            <input type="text" name="companyEmailAddress" id="companyEmailAddress" class="form-control" placeholder="Enter email address">
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Phone number</label>
                            <input type="text" name="companyPhoneNumber" id="companyPhoneNumber" class="form-control" placeholder="Enter phone number">
                        </div>                
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" name="companyLogo" id="companyLogo" placeholder="Browse...">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Active</label>
                            <select name="companyActive" id="companyActive"  class="form-control">
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/company-add.js"></script>
@endsection