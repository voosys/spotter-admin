@extends('layout')

@section('page-title')

<title>Company details | Companies | Spotter</title>

@endsection

@section('content')

<section class="content-header">
    <h1>Company details</h1>
</section>
<section class="content">				
    <div class="box">
        <div class="box-body">	
            <div class="row">
                @if ($userDetails[0]->permission_write == "Yes")
                <div class="col-md-1">
                    <div onclick="updateCompany();" class="btn btn-block btn-success" title="Save">
                        <span class="ion ion-checkmark"/>						
                    </div>
                </div>
                @endif
                @if ($userDetails[0]->permission_delete == "Yes")
                <div class="col-md-1">
                    <div onclick="deleteCompany();" class="btn btn-block btn-danger" title="Delete">
                        <span class="glyphicon glyphicon-remove"/>
                    </div>
                </div>
                @endif
                <div class="col-md-1">
                    <a class="btn btn-block btn-primary" href="{{$sitePath}}companies" title="Back">
                        <span class="ion ion-android-arrow-back"></span>
                    </a>
                </div>				
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">			
            <form name="companyForm" id="companyForm" role="form">
                {{ csrf_field() }}
                <input name="companyIndex" id="companyIndex" type="hidden" value="{{$company[0]->id}}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Company name</label>
                            <input type="text" name="companyName" id="companyName" class="form-control" placeholder="Enter first name" value="{{$company[0]->company_name}}">
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Company website URL</label>
                            <input type="text" name="companyWebsiteURL" id="companyWebsiteURL" class="form-control" placeholder="Enter last name" value="{{$company[0]->company_website_url}}">
                        </div>                
                    </div>
                </div>             
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Email address</label>
                            <input type="text" name="companyEmailAddress" id="companyEmailAddress" class="form-control" placeholder="Enter email address" value="{{$company[0]->company_email_address}}">
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">					
                            <label>Phone number</label>
                            <input type="text" name="companyPhoneNumber" id="companyPhoneNumber" class="form-control" placeholder="Enter phone number" value="{{$company[0]->company_phone_number}}">
                        </div>                
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" name="companyLogo" id="companyLogo" placeholder="Browse...">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Active</label>
                            <select name="companyActive" id="companyActive"  class="form-control">
                                <option value="Yes" @if ($company[0]->company_active == 'Yes') selected="selected" @endif>Yes</option>
                                <option value="No" @if ($company[0]->company_active  == 'No') selected="selected" @endif>No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Created on</label>
                            <input type="text" name="companyCreatedOn" id="companyCreatedOn" class="form-control" value="{{$company[0]->company_created_at}}" disabled="disabled">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Updated on</label>
                            <input type="text" name="companyUpdatedOn" id="companyUpdatedOn" class="form-control" value="{{$company[0]->company_updated_at}}" disabled="disabled">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/company-details.js"></script>
@endsection