@extends('layout')

@section('page-title')

<title>Companies | Spotter</title>

@endsection

@section('style')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.2/css/select.dataTables.min.css">

@endsection

@section('content')

<section class="content-header">
    <h1>Companies</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div class="col-md-1">
                            <div onclick="newCompany();" class="btn btn-block btn-info" title="New company">
                                <i class="ion ion-plus"></i>
                            </div>
                        </div>
                        @endif
                        @if ($userDetails[0]->permission_read == "Yes")
                        <div class="col-md-1">
                            <div onclick="openCompany();" class="btn btn-block btn-primary" title="Open company">
                                <i class="ion ion-eye"></i>
                            </div>
                        </div>
                        @endif
                        @if ($userDetails[0]->permission_delete == "Yes")
                        <div class="col-md-1">
                            <div onclick="deleteCompany();" class="btn btn-block btn-danger" title="Delete company">
                                <i class="ion ion-close"></i>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="table-responsive companies">
                        <form name="companiesForm" id="companiesForm" method="post" action="">
                            {!! csrf_field() !!}
                            <input type="hidden" name="companyIndex" id="companyIndex" value=""/>
                        </form>
                        <table id= "companiesTable" class="table table-hover table-striped" cellspacing="0" width="100%">						
                            <thead>
                                <tr>
                                    <td>
                                        ID
                                    </td>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        Active
                                    </td>
                                    <td>
                                        Created on
                                    </td>
                                    <td>
                                        Updated on
                                    </td>
                                </tr>
                            </thead>																														
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.1.2/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script src="{{$sitePath}}dist/js/helpers/company.js"></script>
@endsection