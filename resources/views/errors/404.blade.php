<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Error | Page not found | Spotter</title>
        <meta name="keywords" content="Spotter" />
        <meta name="description" content="Spotter">
        <meta name="author" content="Spotter">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/vendor.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/theme.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/utility.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/custom.css">

        <link rel="shortcut icon" href="{{$sitePath}}img/favicon.ico">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="minimal error-page" style="background-color:#FFF;">
        <div id="content" class="pn">
            <h1 class="error-title">
                Oh Snap!
            </h1>
            <h2 class="error-subtitle">
                The requested page was not found
            </h2>
            <div class="mid-section">
                <div class="mid-content">
                    <div class="pull-left">
                        <img src="{{$sitePath}}img/logos/logo-publish-white.png" class="img-responsive mt10" alt="logo">
                    </div>
                    <div class="pull-right mt20">
                        <i class="fa fa-facebook-square fs40 text-white mr20"></i>
                        <i class="fa fa-twitter-square fs40 text-white mr20"></i>
                        <i class="fa fa-google-plus-square fs40 text-white mr20"></i>
                        <i class="fa fa-envelope-square fs40 text-white"></i>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>                
        </div>

        <script type="text/javascript" src="{{$sitePath}}vendor/jquery/jquery-1.11.1.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}vendor/jquery/jquery_ui/jquery-ui.min.js"></script> <!-- Bootstrap --> 
        <script type="text/javascript" src="{{$sitePath}}vendor/bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}vendor/plugins/backstretch/jquery.backstretch.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/utility/spin.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/utility/underscore-min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/main.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/ajax.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/custom.js"></script> 
        <script type="text/javascript">
            jQuery(document).ready(function () {

                "use strict";

                Core.init();

                Ajax.init();
            });
        </script>
    </body>
</html>