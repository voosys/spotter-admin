@extends('layout')

@section('page-title')

<title>Un-authorized | Error | Spotter</title>

@endsection

@section('style')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.2/css/select.dataTables.min.css">

@endsection

@section('content')

<section id="content_wrapper">
    <div id="content">
        <div class="row">
            <div class="col-md-12">            
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div>
                            <h4>
                                You are not authorized to access this page.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
@endsection