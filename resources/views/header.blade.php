<header class="navbar navbar-fixed-top">
    <div class="navbar-branding">
        <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
        <a class="navbar-brand" href="{{$sitePath}}dashboard">
            <img src="{{$sitePath}}img/logos/header-logo.png">
        </a>
    </div>
    <div class="navbar-left">
        <div class="navbar-divider"></div>    
    </div>
    <div class="navbar-right">
        <div class="navbar-menus">
            <div class="btn-group" id="alert_menu">
                <button type="button" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicons glyphicons-bell"></span>                    
                </button>
                <ul class="dropdown-menu media-list" role="menu">
                    <li class="dropdown-header">
                        Notifications                        
                    </li>
                    <li class="p15 pb10">
                        <ul class="list-unstyled">                            
                        </ul>
                    </li>
                </ul>
            </div>      
        </div>
    </div>
</header>