<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="keywords" content="Spotter" />
        <meta name="description" content="Spotter">
        <meta name="author" content="Spotter">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        @yield('page-title')

        @include('style', ['sitePath' => $sitePath])
        
        @yield('style')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>

    <body class="dashboard-page">
        <script>

            var boxtest = localStorage.getItem('boxed');

            if (boxtest === 'true') {

                document.body.className += ' boxed-layout';

            }
        </script> 

        @include('header', ['sitePath' => $sitePath])

        <div id="main"> 

            @include('sidebar', ['sitePath' => $sitePath])

            @yield('content')

        </div>

        @include('script', ['sitePath' => $sitePath])

        @yield('script')

    </body>
</html>