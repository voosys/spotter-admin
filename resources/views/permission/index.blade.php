@extends('layout')

@section('page-title')

<title>Permissions | Spotter</title>

@endsection

@section('style')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.2/css/select.dataTables.min.css">

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}permissions">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        Permissions
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left"> 
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="newPermission();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="New permission">
                            <i class="ion ion-plus"></i>
                        </div>
                        @endif
                        @if ($userDetails[0]->permission_read == "Yes")
                        <div onclick="openPermission();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Open permission">
                            <i class="ion ion-eye"></i>
                        </div>
                        @endif                       
                    </div>
                    <div class="pull-right">                        
                    </div>
                    <div class="clearfix">                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">            
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="table-responsive permissions">
                            <form name="permissionsForm" id="permissionsForm" method="post" action="">
                                {!! csrf_field() !!}
                                <input type="hidden" name="permissionIndex" id="permissionIndex" value=""/>
                            </form>
                            <table id= "permissionsTable" class="table table-hover table-striped" cellspacing="0" width="100%">						
                                <thead>
                                    <tr>
                                        <td>
                                            ID
                                        </td>
                                        <td>
                                            Company
                                        </td>
                                        <td>
                                            User
                                        </td>
                                        <td>
                                            Read
                                        </td>
                                        <td>
                                            Write
                                        </td>
                                        <td>
                                            Delete
                                        </td>
                                        <td>
                                            Created on
                                        </td>
                                        <td>
                                            Updated on
                                        </td>
                                    </tr>
                                </thead>																														
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.1.2/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script src="{{$sitePath}}dist/js/helpers/permission.js"></script>
@endsection