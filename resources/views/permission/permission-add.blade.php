@extends('layout')

@section('page-title')

<title>New permission | Permissions | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">    
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}permissions">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        New permission
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">               
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="savePermission();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}permissions" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="newPermissionForm" id="newPermissionForm" role="form">
                            {{ csrf_field() }}
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="companyIndex">
                                            Company
                                        </label>
                                        <select name="companyIndex" id="companyIndex" class="form-control form-control-select">
                                            @foreach ($companies as $company)
                                            <option value="{{$company->id}}">{{$company->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="userIndex">
                                            User
                                        </label>
                                        <select name="userIndex" id="userIndex" class="form-control form-control-select">
                                            @foreach ($users as $user)
                                            <option value="{{$user->id}}">{{$user->user_first_name}} {{$user->user_last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>                
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="permissionRead">
                                            Read
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="permissionRead" id="permissionRead" type="checkbox" checked="checked"/>
                                            <label for="permissionRead"></label>
                                        </div>                                        
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="permissionWrite">
                                            Write
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="permissionWrite" id="permissionWrite" type="checkbox" checked="checked" />
                                            <label for="permissionWrite"></label>
                                        </div>
                                    </div>                
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="permissionDelete">
                                            Delete
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="permissionDelete" id="permissionDelete" type="checkbox" checked="checked" />
                                            <label for="permissionDelete"></label>
                                        </div>
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/permission-add.js"></script>
@endsection