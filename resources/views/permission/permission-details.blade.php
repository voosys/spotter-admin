@extends('layout')

@section('page-title')

<title>Permission details | Permissions | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}permissions">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        Permission details
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">      
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="updatePermission();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}permissions" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="permissionForm" id="permissionForm" role="form">
                            {{ csrf_field() }}
                            <input name="permissionIndex" id="permissionIndex" type="hidden" value="{{$permission[0]->id}}">
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="permissionCompanyName">
                                            Company
                                        </label>
                                        <select name="permissionCompanyName" id="permissionCompanyName" class="form-control form-control-select" disabled="disabled">
                                            <option>{{$permission[0]->company_name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="permissionUserFullName">
                                            User
                                        </label>
                                        <select name="permissionUserFullName" id="permissionUserFullName" class="form-control form-control-select" disabled="disabled">
                                            <option>{{$permission[0]->user_fullname}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="permissionRead">
                                            Read
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="permissionRead" id="permissionRead" type="checkbox" @if ($permission[0]->permission_read == 'Yes') checked="checked" @endif />
                                            <label for="permissionRead"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="permissionWrite">
                                            Write
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="permissionWrite" id="permissionWrite" type="checkbox" @if ($permission[0]->permission_write == 'Yes') checked="checked" @endif />
                                            <label for="permissionWrite"></label>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="permissionDelete">
                                            Delete
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="permissionDelete" id="permissionDelete" type="checkbox" @if ($permission[0]->permission_delete == 'Yes') checked="checked" @endif />
                                            <label for="permissionDelete"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        
                                    </div>
                                </div>
                            </div>                            
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="permissionCreatedOn">
                                            Created on
                                        </label>
                                        <input type="text" name="permissionCreatedOn" id="permissionCreatedOn" class="form-control" value="{{$permission[0]->permission_created_at}}" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="permissionUpdatedOn">
                                            Updated on
                                        </label>
                                        <input type="text" name="permissionUpdatedOn" id="permissionUpdatedOn" class="form-control" value="{{$permission[0]->permission_updated_at}}" disabled="disabled">
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/permission-details.js"></script>
@endsection