@extends('layout')

@section('page-title')

<title>New point type | Point types | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">    
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}point-types">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        New point type
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">               
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="savePointType();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}point-types" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="newPointTypeForm" id="newPointTypeForm" role="form">
                            {{ csrf_field() }}
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="pointTypeName">
                                            Name
                                        </label>
                                        <input class="form-control" id="pointTypeName" name="pointTypeName" type="text" placeholder="Point type name" value=""/>                                    
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="pointTypeActive">
                                            Active
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="pointTypeActive" id="pointTypeActive" type="checkbox" checked="checked" />
                                            <label for="pointTypeActive"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/point-type-add.js"></script>
@endsection