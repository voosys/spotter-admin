@extends('layout')

@section('page-title')

<title>Point type details | Point types | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}point-types">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        Point type details
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">      
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="updatePointType();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}point-types" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                        @if ($userDetails[0]->permission_delete == "Yes")
                        <div onclick="deletePointType();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Delete point type">
                            <i class="ion ion-close"></i>
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="pointTypeForm" id="pointTypeForm" role="form">
                            {{ csrf_field() }}
                            <input name="pointTypeIndex" id="pointTypeIndex" type="hidden" value="{{$point_type[0]->id}}">
                            <div class="row col-md-12">
                                <div class="col-md-5"> 
                                    <div class="form-group">
                                        <label for="pointTypeName">
                                            Name
                                        </label>
                                        <input class="form-control" id="pointTypeName" name="pointTypeName" type="text" placeholder="Point type name" value="{{$point_type[0]->point_type_name}}"/>                                    
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <label for="pointTypeActive">
                                        Active
                                    </label>
                                    <br/>
                                    <div class="switch switch-green switch-inline">
                                        <input name="pointTypeActive" id="pointTypeActive" type="checkbox" @if ($point_type[0]->point_type_active == "Yes") checked @endif />
                                        <label for="pointTypeActive"></label>
                                    </div>    
                                </div>
                            </div> 
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="pointTypeCreatedOn">
                                            Created on
                                        </label>
                                        <input type="text" name="pointTypeCreatedOn" id="pointTypeCreatedOn" class="form-control" value="{{$point_type[0]->point_type_created_at}}" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">                                    
                                    <div class="form-group">                                    
                                        <label for="pointTypeUpdatedOn">
                                            Updated on
                                        </label>
                                        <input type="text" name="pointTypeUpdatedOn" id="pointTypeUpdatedOn" class="form-control" value="{{$point_type[0]->point_type_updated_at}}" disabled="disabled">
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/point-type-details.js"></script>
@endsection