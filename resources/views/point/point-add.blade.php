@extends('layout')

@section('page-title')

<title>New point | Points | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">    
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}points">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        New point
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">               
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="savePoint();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}points" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="newPointForm" id="newPointForm" role="form">
                            {{ csrf_field() }}
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="pointName">
                                            Name
                                        </label>
                                        <input class="form-control" id="pointName" name="pointName" type="text" placeholder="Point name" value=""/>                                    
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="pointTypeIndex">
                                            Type
                                        </label>
                                        <select name="pointTypeIndex" id="pointTypeIndex" class="form-control form-control-select">
                                            @foreach ($pointTypes as $pointType)
                                            <option value="{{$pointType->id}}">{{$pointType->point_type_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>                
                                </div>
                            </div> 
                            <div class="row col-md-12">
                                <div class="col-md-12">                                    
                                    <div class="form-group">
                                        <label for="pointURL">
                                            URL
                                        </label>
                                        <input class="form-control" id="pointURL" name="pointURL" type="text" placeholder="Point URL" value=""/>                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">                                    
                                    <div class="form-group">
                                        <label for="pointLatitude">
                                            Latitude
                                        </label>
                                        <input class="form-control" id="pointLatitude" name="pointLatitude" type="text" placeholder="Latitude" value=""/>                                    
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5"> 
                                    <div class="form-group">
                                        <label for="pointLongitude">
                                            Longitude
                                        </label>
                                        <input class="form-control" id="pointLongitude" name="pointLongitude" type="text" placeholder="Longitude" value=""/>                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="pointActive">
                                            Active
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="pointActive" id="pointActive" type="checkbox" checked="checked" />
                                            <label for="pointActive"></label>
                                        </div>
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/point-add.js"></script>
@endsection