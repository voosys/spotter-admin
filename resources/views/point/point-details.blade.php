@extends('layout')

@section('page-title')

<title>Point details | Points | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}points">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        Point details
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">      
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="updatePoint();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}points" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                        @if ($userDetails[0]->permission_delete == "Yes")
                        <div onclick="deletePoint();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Delete point">
                            <i class="ion ion-close"></i>
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="pointForm" id="pointForm" role="form">
                            {{ csrf_field() }}
                            <input name="pointIndex" id="pointIndex" type="hidden" value="{{$point[0]->id}}">
                            <div class="row col-md-12">
                                <div class="col-md-5">                                    
                                    <div class="form-group">
                                        <label for="pointName">
                                            Name
                                        </label>
                                        <input class="form-control" id="pointName" name="pointName" type="text" placeholder="Point name" value="{{$point[0]->point_name}}"/>                                    
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5"> 
                                    <div class="form-group">
                                        <label for="pointType">
                                            Type
                                        </label>
                                        <select name="pointTypeIndex" id="pointTypeIndex" class="form-control form-control-select">
                                            @foreach ($pointTypes as $pointType)
                                            <option value="{{$pointType->id}}" @if ($point[0]->point_type_id == $pointType->id) selected="selected" @endif>{{$pointType->point_type_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">                                    
                                    <div class="form-group">
                                        <label for="pointURL">
                                            URL
                                        </label>
                                        <input class="form-control" id="pointURL" name="pointURL" type="text" placeholder="Point URL" value="{{$point[0]->point_url}}"/>                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">                                    
                                    <div class="form-group">
                                        <label for="pointLatitude">
                                            Latitude
                                        </label>
                                        <input class="form-control" id="pointLatitude" name="pointLatitude" type="text" placeholder="Latitude" value="{{$point[0]->point_latitude}}"/>                                    
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5"> 
                                    <div class="form-group">
                                        <label for="pointLongitude">
                                            Longitude
                                        </label>
                                        <input class="form-control" id="pointLongitude" name="pointLongitude" type="text" placeholder="Longitude" value="{{$point[0]->point_longitude}}"/>                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <label for="pointActive">
                                        Active
                                    </label>
                                    <br/>
                                    <div class="switch switch-green switch-inline">
                                        <input name="pointActive" id="pointActive" type="checkbox" @if ($point[0]->point_active == "Yes") checked @endif />
                                        <label for="pointActive"></label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="pointCreatedOn">
                                            Created on
                                        </label>
                                        <input type="text" name="pointCreatedOn" id="pointCreatedOn" class="form-control" value="{{$point[0]->point_created_at}}" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="pointUpdatedOn">
                                            Updated on
                                        </label>
                                        <input type="text" name="pointUpdatedOn" id="pointUpdatedOn" class="form-control" value="{{$point[0]->point_updated_at}}" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">                                    
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/point-details.js"></script>
@endsection