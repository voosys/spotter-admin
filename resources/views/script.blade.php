<script type="text/javascript" src="{{$sitePath}}vendor/jquery/jquery-1.11.1.min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/jquery/jquery_ui/jquery-ui.min.js"></script> <!-- Bootstrap --> 
<script type="text/javascript" src="{{$sitePath}}vendor/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/raphael/raphael.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/morris/morris.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/datatables/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/datatables/js/datatables.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/jvectormap/jquery-jvectormap.min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/clndr/clndr.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/clndr/moment.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/jqueryflot/jquery.flot.min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/loadingoverlay/src/loadingoverlay.min.js"></script>
<script type="text/javascript" src="{{$sitePath}}vendor/plugins/zebradialog/src/javascript/zebra_dialog.js"></script>
<script type="text/javascript" src="{{$sitePath}}dist/js/utility/spin.min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}dist/js/utility/underscore-min.js"></script> 
<script type="text/javascript" src="{{$sitePath}}dist/js/main.js"></script> 
<script type="text/javascript" src="{{$sitePath}}dist/js/ajax.js"></script> 
<script type="text/javascript" src="{{$sitePath}}dist/js/custom.js"></script> 
<script type="text/javascript" src="{{$sitePath}}dist/js/script.js"></script> 

<script type="text/javascript">

var sitePath = "{{$sitePath}}";

toastr.options = {
    closeButton: true,
    newestOnTop: true,
    progressBar: true,
    positionClass: 'toast-bottom-right'
};
</script>