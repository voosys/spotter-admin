<aside id="sidebar_left">
    <div class="user-info">
        <div class="media">
            <a class="pull-left" href="{{$sitePath}}user/profile">
                <div class="media-object border border-purple br64 bw2 p2">
                    <img class="br64" src="{{$sitePath}}uploads/users/{{$userDetails[0]->id}}/{{$userDetails[0]->user_profile_picture}}" alt="">
                </div>
            </a>
            <div class="mobile-link">
                <span class="glyphicons glyphicons-show_big_thumbnails"></span>
            </div>
            <div class="media-body">
                <h5 class="media-heading mt5 mbn fw700">
                    {{$userDetails[0]->user_first_name}}&nbsp;{{$userDetails[0]->user_last_name}}
                </h5>
                <div class="media-links fs11">{{$userDetails[0]->user_role}}</div>
            </div>
        </div>
    </div>
    <div class="user-divider"></div>
    <div class="sidebar-menu">
        <ul class="nav">
            <li>
                <a href="{{$sitePath}}points" class="ajax-disable">
                    <span class="ion ion-android-apps"></span>
                    Points
                </a>
            </li>
            <li>
                <a href="{{$sitePath}}point-types" class="ajax-disable" >
                    <span class="ion ion-android-people"></span>
                    Point Types
                </a>
            </li>                    
            <li>
                <a class="accordion-toggle menu-open" href="#">
                    <span class="ion ion-android-settings"></span>
                    <span class="sidebar-title">Settings</span>
                    <span class="caret"></span>
                </a>
                <ul id="settings" class="nav sub-nav">
                    <li>
                        <a href="{{$sitePath}}user/profile" class="ajax-disable">
                            <span class="ion ion-android-person"></span>
                            Profile
                        </a>                        
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{$sitePath}}user/change-password" class="ajax-disable" >
                            <span class="ion ion-locked"></span>
                            Change password
                        </a>
                    </li>
                </ul>
            </li>
            @if ($userDetails[0]->user_role == "Admin" || $userDetails[0]->user_role == "Manager")
            <li>
                <a class="accordion-toggle menu-open" href="#">
                    <span class="ion ion-android-bookmark"></span>
                    <span class="sidebar-title">Manage</span>
                    <span class="caret"></span>
                </a>
                <ul id="settings" class="nav sub-nav">
                    <li>
                        <a href="{{$sitePath}}users" class="ajax-disable" >
                            <span class="ion ion-android-people"></span>
                            Users
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{$sitePath}}permissions" class="ajax-disable" >
                            <span class="ion ion-android-options"></span>
                            Permissions
                        </a>
                    </li>                   
                </ul>
            </li>  
            @endif            
            <li>
                <a href="{{$sitePath}}user/logout" class="ajax-disable sidebar-title">
                    <span class="ion ion-android-exit"></span>
                    Log out
                </a>
            </li>
        </ul>
    </div>
</aside>