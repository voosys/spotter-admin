<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
<link rel="stylesheet" type="text/css" href="{{$sitePath}}css/vendor/fonts/ionicons/ionicons.min.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}vendor/plugins/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}vendor/plugins/zebradialog/src/css/flat/zebra_dialog.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}css/vendor.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}css/theme.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}css/utility.css">
<link rel="stylesheet" type="text/css" href="{{$sitePath}}css/custom.css">

<link rel="shortcut icon" href="{{$sitePath}}img/favicon.ico">