@extends('layout')

@section('page-title')

<title>Change password | Profile | Spotter</title>

@endsection

@section('style')

@endsection

@section('content')

<section id="content_wrapper">
    <h1>Change password</h1>
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}user/profile">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        Change password
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="changeUserPassword();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}users" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Back">
                            <span class="ion ion-android-arrow-back"/>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="changeUserPasswordForm" id="changeUserPasswordForm" role="form">
                            {{ csrf_field() }}
                            <input name="userId" id="userId" type="hidden" value="{{$userDetails[0]->id}}">
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userNewPassword">
                                            New password
                                        </label>
                                        <input type="password" name="userNewPassword" id="userNewPassword" class="form-control" placeholder="Enter new password" value="" >
                                    </div>                                    
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userConfirmNewPassword">
                                            Confirm password
                                        </label>
                                        <input type="password" name="userConfirmNewPassword" id="userConfirmNewPassword" class="form-control" placeholder="Confirm new password" value="">
                                    </div>                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</section>

@endsection

@section('script')

<script src="{{$sitePath}}dist/js/helpers/user-change-password.js"></script>
@endsection