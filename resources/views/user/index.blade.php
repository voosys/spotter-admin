@extends('layout')

@section('page-title')

<title>Users | Spotter</title>

@endsection

@section('style')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.2/css/select.dataTables.min.css">

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}users">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        Users
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">                        
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="newUser();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="New user">
                            <i class="ion ion-plus"></i>
                        </div>
                        @endif
                        @if ($userDetails[0]->permission_read == "Yes")
                        <div onclick="openUser();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Open user">
                            <i class="ion ion-eye"></i>
                        </div>
                        @endif
                        @if ($userDetails[0]->permission_delete == "Yes")
                        <div onclick="deleteUser();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Delete user">
                            <i class="ion ion-close"></i>
                        </div>
                        @endif
                    </div>
                    <div class="pull-right">                        
                    </div>
                    <div class="clearfix">                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="table-responsive users">
                            <form name="usersForm" id="usersForm" method="post" action="">
                                {!! csrf_field() !!}
                                <input type="hidden" name="userIndex" id="userIndex" value=""/>
                            </form>
                            <table id= "usersTable" class="table table-hover table-striped" cellspacing="0" width="100%">						
                                <thead>
                                    <tr>
                                        <td>
                                            ID
                                        </td>
                                        <td>
                                            First name
                                        </td>
                                        <td>
                                            Last name
                                        </td>
                                        <td>
                                            User name
                                        </td>
                                        <td>
                                            Role
                                        </td>
                                        <td>
                                            Active
                                        </td>
                                        <td>
                                            Created on
                                        </td>
                                        <td>
                                            Updated on
                                        </td>
                                    </tr>
                                </thead>																														
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.1.2/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script src="{{$sitePath}}dist/js/helpers/user.js"></script>
@endsection