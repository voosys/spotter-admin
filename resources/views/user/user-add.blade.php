@extends('layout')

@section('page-title')

<title>New user | Users | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}users">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        New user
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="saveUser();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}users" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="newUserForm" id="newUserForm" role="form">
                            {{ csrf_field() }}
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label for="userFirstName">
                                            First name
                                        </label>
                                        <input type="text" name="userFirstName" id="userFirstName" class="form-control" placeholder="Enter first name">
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="userLastName">
                                            Last name
                                        </label>
                                        <input type="text" name="userLastName" id="userLastName" class="form-control" placeholder="Enter last name">
                                    </div>                
                                </div>
                            </div>			
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label  for="userName">
                                            User name
                                        </label>
                                        <input type="text" name="userName" id="userName" class="form-control" placeholder="Enter user name">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label  for="userPassword">
                                            Password
                                        </label>
                                        <input type="password" name="userPassword" id="userPassword" class="form-control" placeholder="Enter password">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="userEmailAddress">
                                            Email address
                                        </label>
                                        <input type="email" name="userEmailAddress" id="userEmailAddress" class="form-control" placeholder="Enter email address">
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="userPhoneNumber">
                                            Phone number
                                        </label>
                                        <input type="text" name="userPhoneNumber" id="userPhoneNumber" class="form-control" placeholder="Enter phone number">
                                    </div>                
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="userCompany">
                                            Company
                                        </label>
                                        <select name="userCompany" id="userCompany"  class="form-control">
                                            @foreach ($companies as $company)
                                            <option value="{{$company->id}}">{{$company->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="userRole">
                                            Role
                                        </label>
                                        <select name="userRole" id="userRole"  class="form-control">
                                            <option value="Admin">Admin</option>
                                            <option value="Manager">Manager</option>
                                            <option value="Editor" selected="selected">Editor</option>
                                        </select>
                                    </div>                
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="userProfilePicture">
                                            Profile picture
                                        </label>
                                        <input type="file" name="userProfilePicture" id="userProfilePicture" placeholder="Browse...">
                                    </div>                
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">					
                                        <label  for="">
                                            Active
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="userActive" id="userActive" type="checkbox" checked="checked"/>
                                            <label for="userActive"></label>
                                        </div>
                                    </div>                
                                </div>
                            </div>
                        </form>  
                    </div>
                </div>                
            </div>
        </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/user-add.js"></script>
@endsection