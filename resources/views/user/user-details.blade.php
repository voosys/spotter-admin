@extends('layout')

@section('page-title')

<title>User details | Users | Spotter</title>

@endsection

@section('content')

<section id="content_wrapper">
    <div id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-active">
                    <a href="{{$sitePath}}users">
                        <span class="glyphicon glyphicon-book"></span>
                        &nbsp;
                        User details
                    </a>
                </li>          
            </ol>
        </div>
        <div class="topbar-right">
            <div class="dashboard-widget-tray">          
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="content-header mb25">
                    <div class="pull-left">
                        @if ($userDetails[0]->permission_write == "Yes")
                        <div onclick="updateUser();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Save">
                            <span class="ion ion-checkmark"/>						
                        </div>
                        <a href="{{$sitePath}}user/change-password" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Reset password">
                            <span class="ion ion-android-lock"></span>
                        </a>
                        @endif
                        @if ($userDetails[0]->permission_delete == "Yes")
                        <div onclick="deleteUser();" class="btn bg-purple2 btn-sm text-light ml15 mr10" title="Delete">
                            <span class="ion ion-trash-a"/>						
                        </div>
                        @endif
                        <a href="{{$sitePath}}users" class="btn bg-purple2 btn-sm text-light ml15 mr10">
                            <span class="ion ion-android-arrow-back"></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form name="userForm" id="userForm" role="form">
                            {{ csrf_field() }}
                            <input name="userIndex" id="userIndex" type="hidden" value="{{$user[0]->id}}">
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userFirstName">
                                            First name
                                        </label>
                                        <input type="text" name="userFirstName" id="userFirstName" class="form-control" placeholder="Enter first name" value="{{$user[0]->user_first_name}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="userLastName">
                                            Last name
                                        </label>
                                        <input type="text" name="userLastName" id="userLastName" class="form-control" placeholder="Enter last name" value="{{$user[0]->user_last_name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userCompany">
                                            Company
                                        </label>
                                        <select name="userCompany" id="userCompany"  class="form-control">
                                            @foreach ($companies as $company)
                                            <option value="{{$company->id}}" @if ($user[0]->company_id == $company->id) selected="selected" @endif >{{$company->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="userRole">
                                            Role
                                        </label>
                                        <select name="userRole" id="userRole"  class="form-control">
                                            <option value="Admin" @if ($user[0]->user_role == 'Admin') selected="selected" @endif>Admin</option>
                                            <option value="Manager" @if ($user[0]->user_role == 'Manager') selected="selected" @endif>Manager</option>
                                            <option value="Designer" @if ($user[0]->user_role == 'Designer') selected="selected" @endif>Designer</option>                                            
                                        </select>
                                    </div>
                                </div>
                            </div>                            
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userName">
                                            User name
                                        </label>
                                        <input type="text" name="userName" id="userName" class="form-control" placeholder="Enter user name" value="{{$user[0]->user_username}}" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="userActive">
                                            Active
                                        </label>
                                        <br/>
                                        <div class="switch switch-green switch-inline">
                                            <input name="userActive" id="userActive" type="checkbox" @if ($user[0]->user_active == 'Yes') checked @endif />
                                                   <label for="userActive"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userEmailAddress">
                                            Email address
                                        </label>
                                        <input type="text" name="userEmailAddress" id="userEmailAddress" class="form-control" placeholder="Enter email address" value="{{$user[0]->user_email_address}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="userPhoneNumber">
                                            Phone number
                                        </label>
                                        <input type="text" name="userPhoneNumber" id="userPhoneNumber" class="form-control" placeholder="Enter phone number" value="{{$user[0]->user_phone_number}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userProfilePicture">
                                            Profile picture
                                        </label>
                                        <input type="file" name="userProfilePicture" id="userProfilePicture" placeholder="Browse...">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="userLastLoggedIn">
                                            Last logged in
                                        </label>
                                        <input type="text" name="userLastLoggedIn" id="userLastLoggedIn" class="form-control" value="{{date('F d, Y', $user[0]->user_last_logged_in)}}" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="userCreatedOn">
                                            Created on
                                        </label>
                                        <input type="text" name="userCreatedOn" id="userCreatedOn" class="form-control" value="{{$user[0]->user_created_at}}" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">                                    
                                        <label for="userUpdatedOn">
                                            Updated on
                                        </label>
                                        <input type="text" name="userUpdatedOn" id="userUpdatedOn" class="form-control" value="{{$user[0]->user_updated_at}}" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{$sitePath}}dist/js/helpers/user-details.js"></script>
@endsection