<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login | Spotter</title>
        <meta name="keywords" content="Spotter" />
        <meta name="description" content="Spotter">
        <meta name="author" content="Spotter">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/vendor.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/theme.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/utility.css">
        <link rel="stylesheet" type="text/css" href="{{$sitePath}}css/custom.css">

        <link rel="shortcut icon" href="{{$sitePath}}img/favicon.ico">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="minimal login-page">
        <script> var boxtest = localStorage.getItem('boxed');
    if (boxtest === 'true') {
        document.body.className += ' boxed-layout';
    }</script> 
        <div id="main">
            <div id="content">
                <div class="row">
                    <div id="page-logo" style="font-size:36px;font-weight:bold;text-align:center;color:#FFF;">
                        LOGIN
                    </div>
                </div>
                <div class="row">
                    <div class="panel-bg">
                        <div class="panel">
                            <form id="signinForm" name="signinForm" method="POST">
                                {{ csrf_field() }}
                                <div class="panel-heading"> <span class="panel-title"> <span class="glyphicon glyphicon-lock text-purple2"></span> Login </span>
                                </div>
                                <div class="panel-body">			
                                    <div class="form-group">
                                        <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span>
                                            <input type="text" name="userName" class="form-control" placeholder="User Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-link"></span> </span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>			
                                </div>
                                <div class="panel-footer">
                                    <span class="text-muted fs12 lh30">
                                        <a href="#">
                                            Forgot Password?
                                        </a>
                                    </span>
                                    <input type="submit" class="btn btn-sm bg-purple2 pull-right" value="Login">
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay-black"></div>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> 
        <script type="text/javascript" src="{{$sitePath}}vendor/jquery/jquery-1.11.1.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}vendor/jquery/jquery_ui/jquery-ui.min.js"></script> <!-- Bootstrap --> 
        <script type="text/javascript" src="{{$sitePath}}vendor/bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}vendor/plugins/backstretch/jquery.backstretch.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/utility/spin.min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/utility/underscore-min.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/main.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/ajax.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/custom.js"></script> 
        <script type="text/javascript" src="{{$sitePath}}dist/js/helpers/user-signin.js"></script> 
        <script type="text/javascript">
            var sitePath = "{{$sitePath}}";
            jQuery(document).ready(function () {

                "use strict";

                Core.init();

                Ajax.init();

                $.backstretch("img/stock/splash/2.jpg");

            });
        </script>
    </body>
</html>